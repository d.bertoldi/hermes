<div align="center">
  <img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/13069939/tumblr_p9s151cZq41rdbmhco1_250.png?width=64" width="150px">
</div>
<br>

# Hermes Ecommerce Platform

HEP is a platform that integrates into your websites a fully functional ecommerce system.

## Requirements (Server-side)
| Component     | Version       | 
| ------------- |:--------------| 
| Java          | 1.8+          |
| Maven         | 3.6+          |
| Tomcat        | 9+            |
| MySQL         | 5.7+          |

## Requirements (Client-side)
Any browser that supports Javascript

## Server Installation
First download the code 

```git clone git@gitlab.com:d.bertoldi/hermes.git```

move to the folder `hermes/` and build the package

```mvn clean install```

this generates a WAR file that can be putted in you Apache Tomcat installation (normally into `webapp/` folder).

## Demo sites

By running on your MySQL instance the `src/main/resources/create.sql` file, you can configure
two demo shops with two different clients ID (namely _CI-01_ and _CI-34_).

Inside the `demo/` folder you can find two websites:
* `index.html` is a host with just a cart but the owner does not have provided the checkout page yet (_CI-01_)
* `index2.html` is a complete ecommerce shop (_CI-34_)

## Testing
In order to perform the unit tests run

```mvn test```

and you want to check the coverage of the tests run

```mvn clean jacoco:prepare-agent install jacoco:report```

That generate's a [JaCoCo](https://www.jacoco.org/) report about the coverage of the unit tests

There are also avaible [JMeter](https://jmeter.apache.org/) tests in the `Hermes.jmx` file.

