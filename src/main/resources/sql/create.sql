-- INIT
CREATE DATABASE IF NOT EXISTS hermes;



-- IMPORTS
INSERT INTO `hermes`.`clients` (`id`, `currency`, `domain`, `deliveryCosts`, `productionEnvironment`) VALUES ('CI-01', 'EUR', '127.0.0.1', '7.50', 0);
INSERT INTO `hermes`.`clients` (`id`, `currency`, `domain`, `deliveryCosts`, `productionEnvironment`) VALUES ('CI-34', 'GBP', '127.0.0.1', '12', 0);




INSERT INTO `hermes`.`payment_modes` (`code`, `client_code`, `providerName`) VALUES ('paypal4CI-34', 'CI-34', 'paypal');


INSERT INTO `hermes`.`paymentmode2properties` (`PaymentModeModel_code`, `property_key`, `property_value`) VALUES ('paypal4CI-34', 'CLIENT_ID', 'AebfLYeMyWvm66SiHS73muV_Wp_4o60pJTlZ2DN3nbxpTcSHLnw6fglcqUu-rk-RGQYVPuak-9mAefLy');
INSERT INTO `hermes`.`paymentmode2properties` (`PaymentModeModel_code`, `property_key`, `property_value`) VALUES ('paypal4CI-34', 'CLIENT_SECRET', 'EBKBurBfovUBT3-P6B0WCmg6JCxUXTS9FC5O-TiHOjCON1uo1zRmpGmpBHimFkyiEpqpFE0ttv-UQ4Fx');
