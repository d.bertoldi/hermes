package it.unimib.inf.ldp.hermes.services;

import javax.transaction.Transactional;

import it.unimib.inf.ldp.hermes.models.CartModel;
import it.unimib.inf.ldp.hermes.models.OrderModel;


@Transactional
public interface PaymentModeService
{

    boolean verify(CartModel cart);

    boolean capture(OrderModel order);

    void setPayPalInformation(CartModel cart, String paypalOrderId);


}
