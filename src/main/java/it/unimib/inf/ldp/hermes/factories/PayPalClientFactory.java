package it.unimib.inf.ldp.hermes.factories;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.Assert;

import com.paypal.core.PayPalEnvironment;
import com.paypal.core.PayPalHttpClient;

import it.unimib.inf.ldp.hermes.models.ClientModel;
import it.unimib.inf.ldp.hermes.models.PaymentModeModel;


public class PayPalClientFactory
{
    private static final Logger LOG = LogManager.getLogger();

    protected static final String CLIENT_ID_KEY = "CLIENT_ID";

    protected static final String CLIENT_SECRET_KEY = "CLIENT_SECRET";

    private PayPalClientFactory()
    {
        // private
    }

    public static PayPalHttpClient build(ClientModel client, PaymentModeModel paypal)
    {
        Assert.notNull(client, "client is null");
        Assert.notNull(paypal, "Payment mode is null");
        Assert.notNull(paypal.getProperties(), "Payment mode has no properties");

        String ppCliendId = paypal.getProperties().get(CLIENT_ID_KEY);
        String ppClientSecret = paypal.getProperties().get(CLIENT_SECRET_KEY);

        if (StringUtils.isAnyEmpty(ppCliendId, ppClientSecret))
        {
            throw new IllegalStateException("Check your paypal configuration in client " + client.getId());
        }

        return new PayPalHttpClient(getEnvironment(client, ppCliendId, ppClientSecret));

    }

    protected static PayPalEnvironment getEnvironment(ClientModel client, String ppCliendId, String ppClientSecret)
    {
        if (client.isProductionEnvironment())
        {
            return new PayPalEnvironment.Live(ppCliendId, ppClientSecret);
        }
        else
        {
            LOG.warn("Client {} is set as sandbox", client.getId());
            return new PayPalEnvironment.Sandbox(ppCliendId, ppClientSecret);
        }

    }

}
