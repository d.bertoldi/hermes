package it.unimib.inf.ldp.hermes.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@RequestMapping("/lib/hermes.js")
@Controller
public class JSLibraryController
{

    @RequestMapping(method = RequestMethod.GET)
    public String get(@RequestParam String clientID, Model model)
    {
        model.addAttribute("clientID", clientID);

        return "jslib";

    }



}
