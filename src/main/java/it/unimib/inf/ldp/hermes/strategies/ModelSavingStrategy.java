package it.unimib.inf.ldp.hermes.strategies;

import javax.transaction.Transactional;

import it.unimib.inf.ldp.hermes.models.AbstractModel;

@Transactional
public interface ModelSavingStrategy<M extends AbstractModel>
{

    void perform(M model);


}
