package it.unimib.inf.ldp.hermes.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;


@Entity
@Table(name = "abstract_payment_info")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class AbstractPaymentInfoModel extends AbstractModel
{

    @Id
    @GeneratedValue
    private long id;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }
}
