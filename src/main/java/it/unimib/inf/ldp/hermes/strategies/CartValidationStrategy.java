package it.unimib.inf.ldp.hermes.strategies;

import it.unimib.inf.ldp.hermes.models.CartModel;


public interface CartValidationStrategy
{

    boolean validate(CartModel cart);

}
