package it.unimib.inf.ldp.hermes.models;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;


@Entity
@Table(name = "clients")
public class ClientModel extends AbstractModel
{
    public static final String ID = "id";

    public static final String DOMAIN = "domain";

    public static final String USERNAME = "username";

    public static final String PASSWORD = "password";

    @Id
    @Column(name = ID)
    private String id;

    @Column(name = DOMAIN)
    private String domain;

    @Column
    private Currency currency;

    @Column
    private BigDecimal deliveryCosts = BigDecimal.ZERO;

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name="client_code")
    @Cascade({CascadeType.ALL})
    private List<PaymentModeModel> supportedPaymentModes;

    @Column
    private boolean productionEnvironment = false;

    @Column(name = USERNAME)
    private String username;

    @Column
    private String password;

    public Currency getCurrency()
    {
        return currency;
    }

    public void setCurrency(Currency currency)
    {
        this.currency = currency;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getDomain()
    {
        return domain;
    }

    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public BigDecimal getDeliveryCosts()
    {
        return deliveryCosts;
    }

    public void setDeliveryCosts(BigDecimal deliveryCosts)
    {
        this.deliveryCosts = deliveryCosts;
    }

    public List<PaymentModeModel> getSupportedPaymentModes()
    {
        return supportedPaymentModes;
    }

    public void setSupportedPaymentModes(List<PaymentModeModel> supportedPaymentModes)
    {
        this.supportedPaymentModes = supportedPaymentModes;
    }

    public boolean isProductionEnvironment()
    {
        return productionEnvironment;
    }

    public void setProductionEnvironment(boolean productionEnvironment)
    {
        this.productionEnvironment = productionEnvironment;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }
}
