package it.unimib.inf.ldp.hermes.services;

import javax.transaction.Transactional;

import it.unimib.inf.ldp.hermes.models.AbstractModel;


@Transactional
public interface ModelService
{
    void refresh(AbstractModel model);

    void save(AbstractModel model);

    void remove(AbstractModel model);

    <T extends AbstractModel> T rawMerge(T model);

}
