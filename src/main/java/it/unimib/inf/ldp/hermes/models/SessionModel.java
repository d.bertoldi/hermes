package it.unimib.inf.ldp.hermes.models;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.springframework.core.annotation.Order;


@Entity
@Table(name = "sessions")
public class SessionModel extends AbstractModel
{
    public static final String ID = "id";

    public static final String CLIENT = "client";

    public static final String CART = "cart";

    @Id
    @Column(name = ID)
    private String id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = CartModel.CODE, name = CART)
    @Cascade({ CascadeType.SAVE_UPDATE })
    private CartModel cart;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = ClientModel.ID, name = CLIENT)
    @Cascade({ CascadeType.SAVE_UPDATE })
    private ClientModel client;

    @Column
    private LocalDateTime expirationTime;

    private SessionModel()
    {
    }

    public SessionModel(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }

    public CartModel getCart()
    {
        return cart;
    }

    public void setCart(CartModel cart)
    {
        this.cart = cart;
    }

    public ClientModel getClient()
    {
        return client;
    }

    public void setClient(ClientModel client)
    {
        this.client = client;
    }

    public LocalDateTime getExpirationTime()
    {
        return expirationTime;
    }

    public void setExpirationTime(LocalDateTime expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null || !getClass().equals(obj.getClass()))
        {
            return false;
        }
        return id.equals(((SessionModel) obj).getId());
    }
}
