package it.unimib.inf.ldp.hermes.strategies;

import java.math.BigDecimal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import it.unimib.inf.ldp.hermes.models.CartModel;

@Component
@Order(value = 1)
public class BasicCartValidationStrategy implements CartValidationStrategy
{
    private static final Logger LOG = LogManager.getLogger();

    @Override
    public boolean validate(CartModel cart)
    {
        if(cart == null)
        {
            LOG.error("Cart is null");
            return false;
        }

        if(CollectionUtils.isEmpty(cart.getEntries()))
        {
            LOG.error("Cart {} has no entries", cart.getCode());
            return false;
        }

        if(BigDecimal.ZERO.compareTo(cart.getTotalPrice()) >= 0)
        {
            LOG.error("Cart {} has invalid total price {}", cart.getCode(),  cart.getTotalPrice());
            return false;
        }

        if(cart.getPaymentMode() == null)
        {
            LOG.error("Cart {} has no payment mode set", cart.getCode());
            return false;
        }

        if(cart.getPaymentInfo() == null)
        {
            LOG.error("Cart {} has no information about payment", cart.getCode());
            return false;
        }

        return true;
    }
}
