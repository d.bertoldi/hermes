package it.unimib.inf.ldp.hermes.web.controllers;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


public abstract class AbstractActionController
{

    protected static ResponseEntity<String> response(Serializable obj, HttpServletRequest request)
    {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*");

        String callback = request.getParameter("callback");

        if (StringUtils.isNotEmpty(callback))
        {
            headers.add(HttpHeaders.CONTENT_TYPE, "text/javascript; charset=UTF-8");

            String jsonp = String.format("%s(%s)", callback, new JSONObject(obj));
            return new ResponseEntity<>(jsonp, headers, HttpStatus.OK);
        }
        else
        {

            return new ResponseEntity<>(obj.toString(), headers, HttpStatus.OK);
        }
    }

}
