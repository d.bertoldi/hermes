package it.unimib.inf.ldp.hermes.strategies;

import it.unimib.inf.ldp.hermes.dtos.CartModification;
import it.unimib.inf.ldp.hermes.models.EntryModel;


public interface UpdateCartEntryStrategy
{

    CartModification perform(EntryModel entry, long quantity);
}
