package it.unimib.inf.ldp.hermes.web.controllers;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.unimib.inf.ldp.hermes.dtos.CheckoutRequest;
import it.unimib.inf.ldp.hermes.models.CartModel;
import it.unimib.inf.ldp.hermes.services.CartService;
import it.unimib.inf.ldp.hermes.services.PaymentModeService;


@RestController
@RequestMapping("/action/checkout/paypal")
public class CheckoutPayPalActionController extends AbstractActionController
{

    private static final Logger LOG = LogManager.getLogger();

    @Autowired
    private CartService cartService;

    @Autowired
    private PaymentModeService paymentModeService;

    @PostMapping(value = "verify", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> verify(@RequestBody CheckoutRequest checkoutRequest, HttpServletRequest request)
    {
        CartModel cart = cartService.getSessionCart();
        paymentModeService.setPayPalInformation(cart, checkoutRequest.getOrderCode());
        if (paymentModeService.verify(cart))
        {
            return response("OK", request);
        }
        else
        {
            return response("KO", request);
        }

    }



}
