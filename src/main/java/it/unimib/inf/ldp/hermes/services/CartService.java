package it.unimib.inf.ldp.hermes.services;

import it.unimib.inf.ldp.hermes.dtos.CartModification;
import it.unimib.inf.ldp.hermes.models.AddressModel;
import it.unimib.inf.ldp.hermes.models.CartModel;
import it.unimib.inf.ldp.hermes.models.EntryModel;
import it.unimib.inf.ldp.hermes.models.ProductModel;

import javax.transaction.Transactional;


@Transactional
public interface CartService
{

        CartModel getSessionCart();

        CartModification addToCart(ProductModel product, long quantity);

        CartModification updateEntry(String entryId, long diff);

        ProductModel toProduct(String productCode, String name, String image, double price);

        EntryModel getEntryById(String entryId);

        void clearCart();

        void setPaymentMode(String paymentModeId);

        void setDeliveryAddress(AddressModel address);

}
