package it.unimib.inf.ldp.hermes.strategies;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import it.unimib.inf.ldp.hermes.models.CartModel;
import it.unimib.inf.ldp.hermes.models.PayPalPaymentInfoModel;


@Component
@Order(value = 3)
public class PayPalCartValidationStrategy implements CartValidationStrategy
{
    private static final Logger LOG = LogManager.getLogger();

    @Override
    public boolean validate(CartModel cart)
    {
        if(cart.getPaymentInfo() instanceof PayPalPaymentInfoModel)
        {
            PayPalPaymentInfoModel payPalPaymentInfo = (PayPalPaymentInfoModel) cart.getPaymentInfo();

            if(StringUtils.isEmpty(payPalPaymentInfo.getRemoteOrderCode()))
            {
                LOG.error("Cart {} has not be transmitted to PayPal", cart.getCode());
                return false;
            }
            return true;

        }
        else
        {
            LOG.debug("Cart {} is not payed with PayPal. Skipping...", cart.getCode());
            return true;
        }
    }
}
