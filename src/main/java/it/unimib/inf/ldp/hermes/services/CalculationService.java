package it.unimib.inf.ldp.hermes.services;

import javax.transaction.Transactional;

import it.unimib.inf.ldp.hermes.models.CartModel;


@Transactional
public interface CalculationService
{

    void calculate(CartModel cart, boolean save);


}
