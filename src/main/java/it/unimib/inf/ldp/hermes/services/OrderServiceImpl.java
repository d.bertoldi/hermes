package it.unimib.inf.ldp.hermes.services;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unimib.inf.ldp.hermes.daos.CartDao;
import it.unimib.inf.ldp.hermes.daos.OrderDao;
import it.unimib.inf.ldp.hermes.models.CartModel;
import it.unimib.inf.ldp.hermes.models.OrderModel;
import it.unimib.inf.ldp.hermes.models.OrderStatus;
import it.unimib.inf.ldp.hermes.models.SessionModel;
import it.unimib.inf.ldp.hermes.strategies.CartValidationStrategy;


@Service
public class OrderServiceImpl implements OrderService
{
    private static final Logger LOG = LogManager.getLogger();

    @Autowired
    private CalculationService calculationService;

    @Autowired
    private ModelService modelService;

    @Autowired
    private CartDao cartDao;

    @Autowired
    private PaymentModeService paymentModeService;

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private SessionService sessionService;

    @Autowired
    List<CartValidationStrategy> cartValidationStrategies;

    @Override
    public OrderModel getOrderByCode(String code)
    {
        if(StringUtils.isEmpty(code))
        {
            LOG.error("Invalid order code");
            return null;
        }
        return orderDao.getOrderByCode(code);
    }

    @Override
    public OrderModel placeOrder(CartModel cart)
    {
        if (!validateCart(cart))
        {
            LOG.error("Order {} not palced", cart.getCode());
            return null;
        }

        calculationService.calculate(modelService.rawMerge(cart), true);

        OrderModel order = cartDao.createOrderFromCart(cart);

        boolean captured = paymentModeService.capture(modelService.rawMerge(order));

        if (captured)
        {
            LOG.info("Order {} is payed", order.getCode());
            order.setOrderStatus(OrderStatus.PAYED);
        }
        else
        {
            LOG.warn("Order {} not payed", order.getCode());
            order.setOrderStatus(OrderStatus.PAYED);
        }

        modelService.save(modelService.rawMerge(order));

        detachCartFromSession();

        return order;
    }

    private void detachCartFromSession()
    {
        SessionModel session = sessionService.getCurrentSession();
        session.setCart(null);
        modelService.save(session);
    }

    private boolean validateCart(CartModel cart)
    {
        return cartValidationStrategies.stream().anyMatch(s -> s.validate(cart));

    }
}
