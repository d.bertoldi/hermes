package it.unimib.inf.ldp.hermes.services;

import it.unimib.inf.ldp.hermes.daos.CartDao;
import it.unimib.inf.ldp.hermes.dtos.CartModification;
import it.unimib.inf.ldp.hermes.models.*;
import it.unimib.inf.ldp.hermes.strategies.AddToCartStrategy;
import it.unimib.inf.ldp.hermes.strategies.UpdateCartEntryStrategy;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Currency;


@Service
public class CartServiceImpl implements CartService
{

    private static final Logger LOG = LogManager.getLogger();

    @Autowired
    private SessionService sessionService;

    @Resource
    private AddToCartStrategy addToCartStrategy;

    @Resource
    private UpdateCartEntryStrategy updateCartEntryStrategy;

    @Autowired
    private ModelService modelService;

    @Autowired
    private CalculationService calculationService;

    @Autowired
    private CartDao cartDao;

    @Override
    public CartModel getSessionCart()
    {
        SessionModel sessionModel = sessionService.getCurrentSession();
        return sessionModel.getCart();
    }

    @Override
    public CartModification addToCart(ProductModel product, long quantity)
    {
        SessionModel currentSession = sessionService.getCurrentSession();
        CartModel cart = currentSession.getCart();
        if (cart == null)
        {
            cart = createCart(currentSession);
        }

        CartModification result = addToCartStrategy.perform(cart, product, quantity);
        calculationService.calculate(cart, true);
        return result;

    }

    @Override
    public CartModification updateEntry(String entryId, long diff)
    {

        EntryModel entry = getEntryById(entryId);
        if(entry == null)
        {
            LOG.entry("No entry with code {}", entryId);
            return null;
        }

        long newQuantity = entry.getQuantity() + diff;

        CartModification result = updateCartEntryStrategy.perform(entry, newQuantity);
        if (result.getQuantityAdded() == 0 && result.getModifiedEntry() != null)
        {
            calculationService.calculate(result.getCart(), true);
            modelService.remove(result.getModifiedEntry());

        }
        else if (result.getQuantityAdded() > 0 && result.getModifiedEntry() != null)
        {
            calculationService.calculate(result.getCart(), true);
        }
        else
        {
            throw new IllegalStateException("Invalid result");
        }
        return result;

    }

    @Override
    public ProductModel toProduct(String productCode, String name, String image, double price)
    {
        Assert.isTrue(StringUtils.isNotBlank(productCode), "Product with blank code");
        Assert.isTrue(price > 0d, String.format("Price for product %s is %s", productCode, price));

        ProductModel product = new ProductModel();
        product.setCode(productCode);
        product.setName(name);
        product.setImage(image);
        product.setPrice(BigDecimal.valueOf(price));
        ClientModel client = sessionService.getCurrentSession().getClient();
        product.setClient(client);
        return product;
    }

    @Override
    public EntryModel getEntryById(String entryId)
    {
        EntryModel entryFound = cartDao.findEntryById(entryId);
        if (entryFound != null)
        {
            CartModel cart = getSessionCart();
            if (cart != null //
                    && entryFound.getOrder() != null //
                    && StringUtils.equals(cart.getCode(), entryFound.getOrder().getCode()))
            {
                return entryFound;
            }
        }

        LOG.warn("No entry found with id ={}", entryId);

        return null;
    }

    @Override
    public void clearCart()
    {

        SessionModel session = sessionService.getCurrentSession();

        CartModel cart = session.getCart();
        if (cart == null)
        {
            return;
        }

        modelService.refresh(session);
        session.setCart(null);
        modelService.save(session);

        cart.setPaymentMode(null);
        cart.setPaymentInfo(null);
        cart.setDeliveryAddress(null);
        modelService.remove(cart);
    }

    @Override
    public void setPaymentMode(String paymentModeId)
    {
        CartModel cart = getSessionCart();
        SessionModel currentSession = sessionService.getCurrentSession();

        if (cart == null)
        {
            LOG.warn("No cart set in session {}", currentSession.getId());
            return;
        }
        ClientModel client = currentSession.getClient();

        PaymentModeModel chosenMode = null;

        String paymentId4Client = getPaymentModeId(paymentModeId, client);

        for (PaymentModeModel paymentMode : client.getSupportedPaymentModes())
        {
            if (StringUtils.equals(paymentId4Client, paymentMode.getCode()))
            {
                chosenMode = paymentMode;
                break;
            }
        }

        if(chosenMode == null)
        {
            LOG.warn("Payment mode {} not supported by client {}", paymentModeId, client.getId());
            return;
        }

        cart.setPaymentMode(chosenMode);
        modelService.save(cart);

        currentSession.setCart(cart);
        modelService.save(currentSession);

    }

    @Override
    public void setDeliveryAddress(AddressModel address)
    {
        CartModel cart = getSessionCart();
        SessionModel currentSession = sessionService.getCurrentSession();

        if (cart == null)
        {
            LOG.warn("No cart set in session {}", currentSession.getId());
            return;
        }

        address.setOrder(cart);
        cart.setDeliveryAddress(address);
        modelService.save(cart);

        currentSession.setCart(cart);
        modelService.save(currentSession);

        LOG.info("Address " + address.toString() + " added to cart with code "+ getSessionCart().getCode());
    }

    protected CartModel createCart(SessionModel currentSession)
    {
        CartModel cart = new CartModel();
        cart.setCurrency((Currency) sessionService.getAttribute("currency"));
        cart.setCode(String.format("%s%d-%s", currentSession.getClient().getId(), Instant.now().toEpochMilli(),
                RandomStringUtils.randomAlphanumeric(5)));
        cart.setClient(currentSession.getClient());
        modelService.save(cart);

        currentSession.setCart(cart);
        modelService.save(currentSession);
        return cart;
    }

    protected String getPaymentModeId(String paymentId, ClientModel clientModel)
    {
        return String.format("%s4%s", paymentId, clientModel.getId());
    }

}
