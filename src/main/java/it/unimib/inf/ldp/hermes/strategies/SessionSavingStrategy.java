package it.unimib.inf.ldp.hermes.strategies;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unimib.inf.ldp.hermes.models.SessionModel;
import it.unimib.inf.ldp.hermes.web.WebSession;

@Service
public class SessionSavingStrategy implements ModelSavingStrategy<SessionModel>
{
    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private WebSession webSession;


    @Override
    public void perform(SessionModel sessionModel)
    {
        webSession.setSession(sessionModel);
        sessionFactory.getCurrentSession().saveOrUpdate(sessionModel);

    }
}
