package it.unimib.inf.ldp.hermes.daos;

import it.unimib.inf.ldp.hermes.models.OrderModel;


public interface OrderDao
{

    OrderModel getOrderByCode(String code);


}
