package it.unimib.inf.ldp.hermes.web.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import it.unimib.inf.ldp.hermes.models.CartModel;
import it.unimib.inf.ldp.hermes.models.OrderModel;
import it.unimib.inf.ldp.hermes.services.CartService;
import it.unimib.inf.ldp.hermes.services.OrderService;
import it.unimib.inf.ldp.hermes.services.SessionService;


@Controller
@RequestMapping("/view/checkout")
public class CheckoutFrameController
{
    private static final Logger LOG = LogManager.getLogger();

    @Autowired
    private CartService cartService;

    @Autowired
    private SessionService sessionService;

    @Autowired
    private OrderService orderService;

    @GetMapping
    public String viewCheckout(Model model)
    {

        cartService.setPaymentMode("paypal");
        CartModel cart = cartService.getSessionCart();

        model.addAttribute("ppClientId", cart.getPaymentMode().getProperties().get("CLIENT_ID"));

        model.addAttribute("cart", cart);
        model.addAttribute("session", sessionService.getCurrentSession());

        return "checkout-frame";
    }

    @GetMapping(value = "placeOrder")
    public RedirectView placeOrder()
    {
        CartModel cart = cartService.getSessionCart();
        try
        {
            OrderModel order = orderService.placeOrder(cart);
            if (order == null)
            {
                LOG.error("Order {} not correctly created", cart.getCode());
            }
            else
            {
                return new RedirectView("thankyou?code=" + order.getCode() + "&clientID=" + cart.getClient().getId());
            }
        }
        catch (Exception e)
        {
            LOG.error("Could not place order {}", cart.getCode(), e);
        }
        return new RedirectView("?error=true");
    }

    @GetMapping("thankyou")
    public String viewOrder(@RequestParam String code, Model model)
    {
        OrderModel order = orderService.getOrderByCode(code);
        model.addAttribute("order", order);

        return "checkout-thankyou";
    }

}
