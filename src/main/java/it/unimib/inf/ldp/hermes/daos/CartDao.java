package it.unimib.inf.ldp.hermes.daos;

import it.unimib.inf.ldp.hermes.models.CartModel;
import it.unimib.inf.ldp.hermes.models.EntryModel;
import it.unimib.inf.ldp.hermes.models.OrderModel;


public interface CartDao
{

    EntryModel findEntryById(String entryId);

    OrderModel createOrderFromCart(CartModel cart);

}
