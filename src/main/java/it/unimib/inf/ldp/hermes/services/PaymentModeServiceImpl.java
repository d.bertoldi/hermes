package it.unimib.inf.ldp.hermes.services;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unimib.inf.ldp.hermes.commands.PaymentCommand;
import it.unimib.inf.ldp.hermes.models.CartModel;
import it.unimib.inf.ldp.hermes.models.OrderModel;
import it.unimib.inf.ldp.hermes.models.PayPalPaymentInfoModel;
import it.unimib.inf.ldp.hermes.models.PaymentModeModel;


@Service
public class PaymentModeServiceImpl implements PaymentModeService
{
    @Autowired
    private SessionService sessionService;

    @Autowired
    private Map<String, PaymentCommand> paymentCommandMap;

    @Autowired
    private ModelService modelService;

    @Override
    public boolean verify(CartModel cart)
    {
        PaymentModeModel chosenPaymentMode = cart.getPaymentMode();

        String commandName = chosenPaymentMode.getProviderName() + "-creation";
        PaymentCommand remoteCreationCommand = paymentCommandMap.get(commandName);

        if(remoteCreationCommand == null)
        {
            throw new IllegalStateException(String.format("Command %s not supported", commandName));
        }

        boolean isVerified = remoteCreationCommand.execute(cart, sessionService.getCurrentSession().getClient());

        if(isVerified)
        {
            modelService.save(modelService.rawMerge(cart));
            return true;
        }
        return false;

    }

    @Override
    public boolean capture(OrderModel order)
    {
        CartModel cart = order.getRelatedCart();
        PaymentModeModel chosenPaymentMode = cart.getPaymentMode();
        String commandName = chosenPaymentMode.getProviderName() + "-capture";
        PaymentCommand captureCommand = paymentCommandMap.get(commandName);

        if(captureCommand == null)
        {
            throw new IllegalStateException(String.format("Command %s not supported", commandName));
        }

        return captureCommand.execute(cart, sessionService.getCurrentSession().getClient());
    }

    @Override
    public void setPayPalInformation(CartModel cart, String paypalOrderId)
    {
        if(cart.getPaymentInfo() != null)
        {
            cart.setPaymentInfo(null);
            modelService.save(cart);
        }

        PayPalPaymentInfoModel paymentInfo = new PayPalPaymentInfoModel();
        paymentInfo.setRemoteOrderCode(paypalOrderId);
        cart.setPaymentInfo(paymentInfo);
        modelService.save(cart);
    }
}
