package it.unimib.inf.ldp.hermes.services;

import javax.transaction.Transactional;

import it.unimib.inf.ldp.hermes.models.SessionModel;

@Transactional
public interface SessionService
{

    SessionModel retrieveSessionByID(String sessionID);

    SessionModel createNewSession(String clientID);

    SessionModel getCurrentSession();

    void refreshSession(SessionModel sessionModel);

    void setAttribute(String key, Object value);

    Object getAttribute(String key);


}
