package it.unimib.inf.ldp.hermes.daos;

import it.unimib.inf.ldp.hermes.models.ProductModel;

import java.util.List;

public interface ProductDao
{
    List<ProductModel> findProductsByClientID(String clientID);

    ProductModel findProductByCodeAndClientID(String productCode, String clientID);

    void createProduct(ProductModel product);

    void updateProduct(ProductModel product);

    void deleteProduct(String productCode, String clientID);
}
