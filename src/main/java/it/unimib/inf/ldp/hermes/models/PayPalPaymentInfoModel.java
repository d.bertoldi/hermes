package it.unimib.inf.ldp.hermes.models;

import javax.persistence.Column;
import javax.persistence.Entity;


@Entity
public class PayPalPaymentInfoModel extends AbstractPaymentInfoModel
{

    @Column
    private String remoteOrderCode;

    public String getRemoteOrderCode()
    {
        return remoteOrderCode;
    }

    public void setRemoteOrderCode(String remoteOrderCode)
    {
        this.remoteOrderCode = remoteOrderCode;
    }
}
