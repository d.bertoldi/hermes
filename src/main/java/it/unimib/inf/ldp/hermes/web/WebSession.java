package it.unimib.inf.ldp.hermes.web;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import it.unimib.inf.ldp.hermes.models.SessionModel;


@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class WebSession
{

    private SessionModel session;

    private ConcurrentMap<String, Object> attributes = new ConcurrentHashMap<>();

    public SessionModel getSession()
    {
        return session;
    }

    public void setSession(SessionModel session)
    {
        this.session = session;
    }

    public void setAttribute(String key, Object value)
    {
        attributes.put(key, value);
    }

    public Object getAttribute(String key)
    {
        return attributes.getOrDefault(key, null);
    }
}
