package it.unimib.inf.ldp.hermes.web.controllers;

import it.unimib.inf.ldp.hermes.dtos.BackofficeResponse;
import it.unimib.inf.ldp.hermes.exceptions.ProductNotFoundException;
import it.unimib.inf.ldp.hermes.models.ClientModel;
import it.unimib.inf.ldp.hermes.models.ProductModel;
import it.unimib.inf.ldp.hermes.services.BackofficeSessionService;
import it.unimib.inf.ldp.hermes.services.ProductService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/backoffice")
public class BackofficeController
{
    private static final String BACKOFFICE = "/backoffice";
    private static final Logger LOG = LogManager.getLogger(BackofficeController.class);

    @Autowired
    BackofficeSessionService sessionService;

    @Autowired
    ProductService productService;

    @RequestMapping(method = RequestMethod.GET)
    public String showBackoffice(Model model, HttpServletRequest request)
    {
        if (sessionService.getClientFromSession(request) == null)
        {
            return showLoginPage(model, false);
        }
        return "backoffice-home";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String performLogin(@ModelAttribute(name = "username") String username,
            @ModelAttribute(name = "password") String password, Model model,
            HttpServletRequest request)
    {
        if (sessionService.getClientFromSession(request) == null &&
                !sessionService.performLogin(username, password, request))
        {
            return showLoginPage(model, true);
        }

        LOG.info("Buyer with username {} logged in.", username);

        return "backoffice-home";
    }

    private String showLoginPage(Model model, boolean showLoginError)
    {
        model.addAttribute("showLoginError", showLoginError);
        return "backoffice-login";
    }

    @RequestMapping(path = "/logout", method = RequestMethod.GET)
    public String performLogout(HttpServletRequest request)
    {
        ClientModel client = sessionService.performLogout(request);
        if (client != null)
        {
            LOG.info("Buyer with username {} logged in.", client.getUsername());
        }

        return "redirect:/backoffice";
    }

    @ResponseBody
    @RequestMapping(path = "/products", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProductModel> getProducts(HttpServletRequest request)
    {
        ClientModel client = sessionService.getClientFromSession(request);
        if (client == null)
        {
            return Collections.EMPTY_LIST;
        }

        return productService.getAllProductsForClientID(client.getId());
    }

    @ResponseBody
    @RequestMapping(path = "/product/{productCode}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ProductModel getProduct(@PathVariable("productCode") String productCode, HttpServletRequest request)
    {
        ClientModel client = sessionService.getClientFromSession(request);
        if (client == null)
        {
            return null;
        }

        return productService.getProductFromCodeAndClientID(productCode, client.getId());
    }

    @RequestMapping(path = "/createProductForm", method = RequestMethod.GET)
    public String showProductForm(Model model, HttpServletRequest request)
    {
        return showProductForm("createProduct", model, request);
    }

    @ResponseBody
    @RequestMapping(path = "/createProduct", method = RequestMethod.POST, produces = "application/json")
    public BackofficeResponse createProduct(@ModelAttribute("productModel") ProductModel product, HttpServletRequest request)
    {
        ClientModel client = sessionService.getClientFromSession(request);
        if (client == null)
        {
            // handle client not logged in
            return notLoggedUserInResponse(request);
        }

        product.setClient(client);
        try
        {
            productService.createProduct(product);
            return new BackofficeResponse(BackofficeResponse.Result.OK, "", "");
        }
        catch(DataIntegrityViolationException dive)
        {
            LOG.warn("ProductModel.ID - ClientModel.ID uniqueness has been violated");
        }

        return new BackofficeResponse(BackofficeResponse.Result.NOK, "Error creating product", "");
    }

    @RequestMapping(path = "/updateProductForm/{productCode}", method = RequestMethod.GET)
    public String showUpdateProductForm(@PathVariable("productCode") @NotNull String productCode, Model model,
            HttpServletRequest request)
    {
        return showProductForm("updateProduct/" + productCode, productCode, model, request);
    }

    @ResponseBody
    @RequestMapping(path = "/updateProduct/{productCode}")
    public BackofficeResponse updateProduct(@PathVariable("productCode") @NotNull String productCode,
            @ModelAttribute("productModel") ProductModel product, HttpServletRequest request)
    {
        ClientModel client = sessionService.getClientFromSession(request);
        if (client == null)
        {
            // handle client not logged in
            return notLoggedUserInResponse(request);
        }

        product.setCode(productCode);
        product.setClient(client);
        try
        {
            productService.updateProduct(product);
            String redirectUrl = getOriginUrl(request) + BACKOFFICE + "/products";
            return new BackofficeResponse(BackofficeResponse.Result.OK, "", redirectUrl);
        }
        catch(Exception e)
        {
            LOG.error("Exception thrown: ", e);
            LOG.info("Couple ProductModel.ID - ClientModel.ID not found, update rejected");
        }

        return new BackofficeResponse(BackofficeResponse.Result.NOK, "Error updating product", "");
    }

    @ResponseBody
    @RequestMapping(path = "/deleteProduct/{productCode}", method = RequestMethod.DELETE)
    public BackofficeResponse deleteProduct(@PathVariable("productCode") String productCode, HttpServletRequest request)
    {
        ClientModel client = sessionService.getClientFromSession(request);
        if (client == null)
        {
            // handle client not logged in
            return notLoggedUserInResponse(request);
        }

        try
        {
            productService.deleteProduct(productCode, client.getId());
        }
        catch(ProductNotFoundException pnfe)
        {
            return new BackofficeResponse(BackofficeResponse.Result.NOK, "", "");
        }

        return new BackofficeResponse(BackofficeResponse.Result.OK, "", "");
    }

    private String showProductForm(String action, Model model, HttpServletRequest request)
    {
        return showProductForm(action, null, model, request);
    }

    private String showProductForm(String action, String productCode, Model model, HttpServletRequest request)
    {
        model.addAttribute("originUrl", getOriginUrl(request));
        model.addAttribute("action", action);
        model.addAttribute("updateProduct", productCode == null ? Boolean.FALSE : Boolean.TRUE);
        model.addAttribute("productCode", productCode);
        return "addProductForm";
    }

    private BackofficeResponse notLoggedUserInResponse(HttpServletRequest request)
    {
        BackofficeResponse result = new BackofficeResponse();
        result.setResult(BackofficeResponse.Result.NOK);
        result.setRedirectUrl(getOriginUrl(request) + "/backoffice");
        return result;
    }

    private String getOriginUrl(HttpServletRequest request)
    {
        String pattern = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
        String url = request.getRequestURL().toString();

        String result = url;
        if (!pattern.isEmpty())
        {
            int match = url.indexOf(pattern);
            if (match > 0)
            {
                result = url.substring(0, match);
            }
        }

        return result;
    }

}
