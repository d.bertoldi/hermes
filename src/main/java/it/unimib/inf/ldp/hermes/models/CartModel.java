package it.unimib.inf.ldp.hermes.models;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;


@Entity
@Table(name = "carts")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class CartModel extends AbstractModel
{

    public static final String CODE = "code";

    @Id
    @Column(name = CODE)
    private String code;

    @OneToMany(fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name="cart_code")
    @Cascade(CascadeType.ALL)
    private List<EntryModel> entries = new ArrayList<>();

    @Column
    private BigDecimal totalPrice;

    @Column
    private BigDecimal deliveryCost;

    @Column
    private Currency currency;

    @OneToOne(mappedBy = "order", orphanRemoval = true)
    @Cascade(CascadeType.ALL)
    private AddressModel deliveryAddress;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = PaymentModeModel.CODE, name = "paymentMode")
    @Cascade({ CascadeType.ALL })
    private PaymentModeModel paymentMode;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "id")
    @Cascade({ CascadeType.ALL })
    private AbstractPaymentInfoModel paymentInfo;

    @ManyToOne
    @JoinColumn(referencedColumnName = ClientModel.ID)
    @Cascade({ CascadeType.SAVE_UPDATE })
    private ClientModel client;

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public List<EntryModel> getEntries()
    {
        return entries;
    }

    public void setEntries(List<EntryModel> entries)
    {
        this.entries = entries;
    }

    public BigDecimal getTotalPrice()
    {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice)
    {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getDeliveryCost()
    {
        return deliveryCost;
    }

    public void setDeliveryCost(BigDecimal deliveryCost)
    {
        this.deliveryCost = deliveryCost;
    }

    public Currency getCurrency()
    {
        return currency;
    }

    public void setCurrency(Currency currency)
    {
        this.currency = currency;
    }

    public AddressModel getDeliveryAddress()
    {
        return deliveryAddress;
    }

    public void setDeliveryAddress(AddressModel deliveryAddress)
    {
        this.deliveryAddress = deliveryAddress;
    }

    public PaymentModeModel getPaymentMode()
    {
        return paymentMode;
    }

    public void setPaymentMode(PaymentModeModel paymentMode)
    {
        this.paymentMode = paymentMode;
    }

    public AbstractPaymentInfoModel getPaymentInfo()
    {
        return paymentInfo;
    }

    public void setPaymentInfo(AbstractPaymentInfoModel paymentInfo)
    {
        this.paymentInfo = paymentInfo;
    }

    public ClientModel getClient()
    {
        return client;
    }

    public void setClient(ClientModel client)
    {
        this.client = client;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null || !getClass().equals(obj.getClass()))
        {
            return false;
        }
        return code.equals(((CartModel) obj).getCode());
    }
}
