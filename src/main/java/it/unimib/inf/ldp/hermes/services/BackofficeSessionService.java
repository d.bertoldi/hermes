package it.unimib.inf.ldp.hermes.services;

import it.unimib.inf.ldp.hermes.models.ClientModel;

import javax.servlet.http.HttpServletRequest;

public interface BackofficeSessionService
{
    boolean performLogin(String username, String password, HttpServletRequest request);

    ClientModel performLogout(HttpServletRequest request);

    ClientModel getClientFromSession(HttpServletRequest request);
}
