package it.unimib.inf.ldp.hermes.models;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;


@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "order_entries")
public class EntryModel extends AbstractModel
{

    public static final String ID = "id";

    @Id
    @Column(name = ID)
    private String id;

    @ManyToOne(fetch = FetchType.EAGER)
    @Cascade({ CascadeType.ALL})
    private CartModel order;

    @ManyToOne
    @JoinColumn(name="entry_product_code")
    @Cascade({CascadeType.SAVE_UPDATE})
    private ProductModel product;

    @Column
    private long quantity;

    @Column
    private BigDecimal totalPrice;

    @Column
    private BigDecimal basePrice;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public CartModel getOrder()
    {
        return order;
    }

    public void setOrder(CartModel order)
    {
        this.order = order;
    }

    public ProductModel getProduct()
    {
        return product;
    }

    public void setProduct(ProductModel product)
    {
        this.product = product;
    }

    public long getQuantity()
    {
        return quantity;
    }

    public void setQuantity(long quantity)
    {
        this.quantity = quantity;
    }

    public BigDecimal getTotalPrice()
    {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice)
    {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getBasePrice()
    {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice)
    {
        this.basePrice = basePrice;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null || !getClass().equals(obj.getClass()))
        {
            return false;
        }
        return id.equals(((EntryModel) obj).getId());
    }
}
