package it.unimib.inf.ldp.hermes.strategies;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import it.unimib.inf.ldp.hermes.dtos.CartModification;
import it.unimib.inf.ldp.hermes.models.CartModel;
import it.unimib.inf.ldp.hermes.models.EntryModel;
import it.unimib.inf.ldp.hermes.models.ProductModel;


@Component
public class CommerceAddToCartStrategy implements AddToCartStrategy
{

    private static final Logger LOG = LogManager.getLogger();


    public CartModification perform(CartModel cart, ProductModel product, long quantity)
    {
        if (quantity < 1)
        {
            LOG.error("Cannot add non-positive quantity of product {} into cart {}", product.getCode(), cart.getCode());
            return new CartModification(product, cart, 0, null);
        }

        List<EntryModel> entries = cart.getEntries();

        EntryModel theEntry = null;
        for (EntryModel entry : entries)
        {
            if (StringUtils.equals(product.getCode(), entry.getProduct().getCode()))
            {
                theEntry = entry;
                break;

            }
        }
        if (theEntry == null)
        {
            theEntry = createNewEntry(cart, product, quantity);
            entries.add(theEntry);
            cart.setEntries(entries);
        }
        else
        {
            addToEntry(theEntry, quantity);
        }

        return new CartModification(product, cart, quantity, theEntry);

    }

    private EntryModel createNewEntry(CartModel cart, ProductModel product, long quantity)
    {
        EntryModel entry = new EntryModel();
        entry.setId(cart.getCode() + "." + product.getCode());
        entry.setProduct(product);
        entry.setQuantity(quantity);
        entry.setBasePrice(product.getPrice());
        entry.setOrder(cart);
        return entry;
    }

    private void addToEntry(EntryModel entry, long quantity)
    {
        long currentQuantity = entry.getQuantity();
        entry.setQuantity(currentQuantity + quantity);
        entry.setBasePrice(entry.getProduct().getPrice());
    }

}
