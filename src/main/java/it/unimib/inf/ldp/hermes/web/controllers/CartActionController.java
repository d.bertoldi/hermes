package it.unimib.inf.ldp.hermes.web.controllers;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.unimib.inf.ldp.hermes.dtos.CartActionResponse;
import it.unimib.inf.ldp.hermes.dtos.CartModification;
import it.unimib.inf.ldp.hermes.models.ProductModel;
import it.unimib.inf.ldp.hermes.services.CartService;


@RestController
@RequestMapping("/action/cart")
public class CartActionController extends AbstractActionController
{

    private static final Logger LOG = LogManager.getLogger();

    @Autowired
    private CartService cartService;

    @GetMapping(value = "add")
    public ResponseEntity<String> addToCart(@RequestParam String productCode, @RequestParam String productName,
            @RequestParam String productImg, @RequestParam double productPrice, @RequestParam long quantity,
            HttpServletRequest request)
    {
        LOG.info("adding product with code {}, name {}, img {} and price {}", productCode, productName, productImg, productPrice);

        ProductModel productModel = cartService.toProduct(productCode, productName, productImg, productPrice);
        CartModification cartModification = cartService.addToCart(productModel, quantity);

        return response(new CartActionResponse(cartModification.getQuantityAdded()), request);
    }

    @GetMapping(value = "update")
    public ResponseEntity<String> updateEntry(@RequestParam String entryId, @RequestParam long diff, HttpServletRequest request)
    {
        LOG.info("Updating {} with diff = {}", entryId, diff);

        CartModification cartModification = cartService.updateEntry(entryId, diff);

        return response(new CartActionResponse(cartModification == null ? 0 : cartModification.getQuantityAdded()), request);
    }

    @GetMapping("clear")
    public void clear()
    {
        LOG.info("Clearing cart");

        cartService.clearCart();
    }

}
