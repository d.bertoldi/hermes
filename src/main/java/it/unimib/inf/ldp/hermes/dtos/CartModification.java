package it.unimib.inf.ldp.hermes.dtos;

import it.unimib.inf.ldp.hermes.models.CartModel;
import it.unimib.inf.ldp.hermes.models.EntryModel;
import it.unimib.inf.ldp.hermes.models.ProductModel;


public class CartModification
{

    private long quantityAdded;

    private ProductModel product;

    private CartModel cart;

    private EntryModel modifiedEntry;

    public CartModification(ProductModel product, CartModel cart, long quatityAdded, EntryModel entry)
    {
        this.product = product;
        this.cart = cart;
        this.quantityAdded = quatityAdded;
        this.modifiedEntry = entry;
    }

    public long getQuantityAdded()
    {
        return quantityAdded;
    }

    public ProductModel getProduct()
    {
        return product;
    }

    public CartModel getCart()
    {
        return cart;
    }

    public EntryModel getModifiedEntry()
    {
        return modifiedEntry;
    }
}
