package it.unimib.inf.ldp.hermes.services;

import it.unimib.inf.ldp.hermes.daos.ProductDao;
import it.unimib.inf.ldp.hermes.models.ProductModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService
{
    @Autowired
    private ProductDao productDao;

    @Override
    public List<ProductModel> getAllProductsForClientID(String clientID) {
        return productDao.findProductsByClientID(clientID);
    }

    @Override
    public ProductModel getProductFromCodeAndClientID(String productCode, String clientID)
    {
        return productDao.findProductByCodeAndClientID(productCode, clientID);
    }

    @Override
    public void createProduct(ProductModel product)
    {
        productDao.createProduct(product);
    }

    @Override
    public void updateProduct(ProductModel product)
    {
        productDao.updateProduct(product);
    }

    @Override
    public void deleteProduct(String productCode, String clientID)
    {
        productDao.deleteProduct(productCode, clientID);
    }
}
