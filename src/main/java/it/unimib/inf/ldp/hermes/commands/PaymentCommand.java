package it.unimib.inf.ldp.hermes.commands;

import it.unimib.inf.ldp.hermes.models.CartModel;
import it.unimib.inf.ldp.hermes.models.ClientModel;


public interface PaymentCommand
{

    boolean execute(CartModel cart, ClientModel client);

}
