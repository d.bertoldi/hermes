package it.unimib.inf.ldp.hermes.services;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unimib.inf.ldp.hermes.models.AbstractModel;
import it.unimib.inf.ldp.hermes.models.SessionModel;
import it.unimib.inf.ldp.hermes.strategies.ModelSavingStrategy;


@Service
public class ModelServiceImpl implements ModelService
{

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private ModelSavingStrategy<SessionModel> sessionSavingStrategy;

    @Override
    public void refresh(AbstractModel model)
    {
        sessionFactory.getCurrentSession().refresh(model);
    }

    @Override
    public void save(AbstractModel model)
    {
        if (model instanceof SessionModel)
        {
            sessionSavingStrategy.perform((SessionModel) model);
        }
        else
        {
            sessionFactory.getCurrentSession().saveOrUpdate(model);
        }
        sessionFactory.getCurrentSession().flush();
    }

    @Override
    public void remove(AbstractModel model)
    {
        sessionFactory.getCurrentSession().delete(model);
        sessionFactory.getCurrentSession().flush();
    }

    @Override
    public <T extends AbstractModel> T rawMerge(T model)
    {
        return (T) sessionFactory.getCurrentSession().merge(model);



    }

}
