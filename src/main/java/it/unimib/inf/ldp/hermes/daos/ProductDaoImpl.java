package it.unimib.inf.ldp.hermes.daos;

import it.unimib.inf.ldp.hermes.exceptions.ProductNotFoundException;
import it.unimib.inf.ldp.hermes.models.ClientModel;
import it.unimib.inf.ldp.hermes.models.ProductModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;
import java.util.List;

@Repository
public class ProductDaoImpl extends AbstractDao implements ProductDao
{
    private static final Logger LOG = LogManager.getLogger(ProductDaoImpl.class);

    @Override
    public List<ProductModel> findProductsByClientID(String clientID)
    {
        CriteriaBuilder builder = getSession().getCriteriaBuilder();
        CriteriaQuery<ProductModel> query = builder.createQuery(ProductModel.class);
        Root<ProductModel> root = query.from(ProductModel.class);
        query.select(root).where(builder.equal(root.join("client").get(ClientModel.ID), clientID));

        Query<ProductModel> q = getSession().createQuery(query);

        return q.getResultList();
    }

    @Override
    public ProductModel findProductByCodeAndClientID(String productCode, String clientID)
    {
        CriteriaBuilder builder = getSession().getCriteriaBuilder();
        CriteriaQuery<ProductModel> query = builder.createQuery(ProductModel.class);
        Root<ProductModel> root = query.from(ProductModel.class);
        Predicate codeEquality = builder.equal(root.get(ProductModel.CODE), productCode);
        Predicate clientEquality = builder.equal(root.join("client").get(ClientModel.ID), clientID);
        query.select(root).where(builder.and(codeEquality, clientEquality));

        Query<ProductModel> q = getSession().createQuery(query);

        List<ProductModel> result = q.getResultList();
        if (result != null && result.size() == 1)
        {
            return result.get(0);
        }
        return null;
    }

    @Override
    public void createProduct(ProductModel product)
    {
        getSession().save(product);
    }

    @Override
    public void updateProduct(ProductModel product)
    {
        getSession().update(product);
    }

    @Override
    public void deleteProduct(String productId, String clientID)
    {
        ProductModel product = findProductByCodeAndClientID(productId, clientID);
        if (product == null)
        {
            LOG.error("Product with code {} and clientID {} not found!", productId, clientID);
            throw new ProductNotFoundException();
        }
        else
        {
            getSession().delete(product);
        }
    }

}
