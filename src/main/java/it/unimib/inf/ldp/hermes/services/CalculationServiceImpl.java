package it.unimib.inf.ldp.hermes.services;

import java.math.BigDecimal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unimib.inf.ldp.hermes.models.CartModel;
import it.unimib.inf.ldp.hermes.models.ClientModel;
import it.unimib.inf.ldp.hermes.models.EntryModel;
import it.unimib.inf.ldp.hermes.models.ProductModel;


@Service
public class CalculationServiceImpl implements CalculationService
{

    private static final Logger LOG = LogManager.getLogger();

    @Autowired
    private SessionService sessionService;

    @Autowired
    private ModelService modelService;

    @Override
    public void calculate(CartModel cart, boolean save)
    {
        if (cart == null)
        {
            LOG.error("Tried to calculate null cart");
            return;
        }

        BigDecimal total = BigDecimal.ZERO;

        for (EntryModel entry : cart.getEntries())
        {
            total = partialTotalAndUpdateEntry(entry, total);
        }

        ClientModel clientModel = sessionService.getCurrentSession().getClient();
        BigDecimal deliveryCosts = clientModel.getDeliveryCosts();

        total = total.add(deliveryCosts);

        cart.setTotalPrice(total);
        cart.setDeliveryCost(deliveryCosts);

        if (save)
        {
            modelService.save(cart);
        }

    }

    private BigDecimal partialTotalAndUpdateEntry(EntryModel entry, BigDecimal total)
    {
        ProductModel product = entry.getProduct();
        BigDecimal basePrice = product.getPrice();

        BigDecimal entryTotalPrice = basePrice.multiply(BigDecimal.valueOf(entry.getQuantity()));
        entry.setTotalPrice(entryTotalPrice);
        return entryTotalPrice.add(total);
    }
}
