package it.unimib.inf.ldp.hermes.services;

import javax.transaction.Transactional;

import it.unimib.inf.ldp.hermes.models.CartModel;
import it.unimib.inf.ldp.hermes.models.OrderModel;


@Transactional
public interface OrderService
{

    OrderModel getOrderByCode(String code);

    OrderModel placeOrder(CartModel cart);

}
