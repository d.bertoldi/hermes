package it.unimib.inf.ldp.hermes.strategies;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import it.unimib.inf.ldp.hermes.models.AddressModel;
import it.unimib.inf.ldp.hermes.models.CartModel;


@Component
@Order(value = 2)
public class AddressCartValidationStrategy implements CartValidationStrategy
{
    private static final Logger LOG = LogManager.getLogger();

    @Override
    public boolean validate(CartModel cart)
    {
        if (cart.getDeliveryAddress() == null)
        {
            LOG.error("Cart {} has no address", cart.getCode());
            return false;
        }

        AddressModel address = cart.getDeliveryAddress();
        if (StringUtils.isAnyEmpty(address.getFirstName(), address.getLastName(), address.getCountry(), address.getCity(),
                address.getZipcode(), address.getStreet()))
        {
            LOG.error("Cart {} misses information in its address.\nCheck: {}", cart.getCode(), address.toString());
            return false;
        }
        return true;
    }
}