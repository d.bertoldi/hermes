package it.unimib.inf.ldp.hermes.commands;

import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.braintreepayments.http.HttpResponse;
import com.braintreepayments.http.serializer.Json;
import com.paypal.orders.AddressPortable;
import com.paypal.orders.Order;
import com.paypal.orders.OrdersGetRequest;
import com.paypal.orders.PurchaseUnit;

import it.unimib.inf.ldp.hermes.factories.PayPalClientFactory;
import it.unimib.inf.ldp.hermes.models.AddressModel;
import it.unimib.inf.ldp.hermes.models.CartModel;
import it.unimib.inf.ldp.hermes.models.ClientModel;
import it.unimib.inf.ldp.hermes.models.PayPalPaymentInfoModel;
import it.unimib.inf.ldp.hermes.services.ModelService;


@Component("paypal-creation")
public class PayPalOrderCreationCommand implements PaymentCommand
{
    private static final Logger LOG = LogManager.getLogger();

    @Autowired
    private ModelService modelService;

    @Override
    public boolean execute(CartModel cart, ClientModel client)
    {
        cart = modelService.rawMerge(cart);
        PayPalPaymentInfoModel paymentInfo = (PayPalPaymentInfoModel) cart.getPaymentInfo();
        OrdersGetRequest request = new OrdersGetRequest(paymentInfo.getRemoteOrderCode());

        try
        {
            HttpResponse<Order> response = PayPalClientFactory.build(client, cart.getPaymentMode()).execute(request);

            LOG.info(new JSONObject(new Json().serialize(response.result())).toString(4));

            List<PurchaseUnit> pus = response.result().purchaseUnits();
            double sum = pus.stream().mapToDouble(p -> Double.parseDouble(p.amount().value())).sum();

            if (sum != cart.getTotalPrice().doubleValue())
            {
                LOG.error("Unexpected sum for cart {} - {} vs {}", cart.getCode(), cart.getTotalPrice().doubleValue(), sum);
                return false;
            }

            associateAddress(cart, response.result());

        }
        catch (IOException e)
        {
            LOG.error("Error while contacting PayPal for order {}", cart.getCode(), e);
            return false;
        }

        LOG.info("Order {} checked", cart.getCode());

        return true;

    }

    protected void associateAddress(CartModel cart, Order ppOrder)
    {

        PurchaseUnit pu = ppOrder.purchaseUnits().get(0);
        AddressPortable ppAddress = pu.shipping().addressPortable();

        AddressModel addressModel;
        if (cart.getDeliveryAddress() == null)
        {
            addressModel = new AddressModel();
            addressModel.setOrder(cart);
        }
        else
        {
            addressModel = cart.getDeliveryAddress();
        }
        addressModel.setCity(ppAddress.adminArea1());
        addressModel.setStreet(ppAddress.addressLine1());
        addressModel.setZipcode(ppAddress.postalCode());
        addressModel.setCountry(ppAddress.countryCode());


        addressModel.setFirstName(ppOrder.payer().name().givenName());
        addressModel.setLastName(ppOrder.payer().name().surname());
        addressModel.setEmail(ppOrder.payer().emailAddress());

        cart.setDeliveryAddress(addressModel);

        modelService.save(cart);
    }

}
