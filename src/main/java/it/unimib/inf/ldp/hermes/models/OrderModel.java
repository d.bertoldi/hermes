package it.unimib.inf.ldp.hermes.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;


@Entity
@Table(name = "orders")
public class OrderModel extends AbstractModel
{

    @Id
    @Column
    private String code;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = CartModel.CODE)
    @Cascade(CascadeType.ALL)
    private CartModel relatedCart;

    @Enumerated(EnumType.ORDINAL)
    private OrderStatus orderStatus;

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public CartModel getRelatedCart()
    {
        return relatedCart;
    }

    public void setRelatedCart(CartModel relatedCart)
    {
        this.relatedCart = relatedCart;
    }

    public OrderStatus getOrderStatus()
    {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus)
    {
        this.orderStatus = orderStatus;
    }
}
