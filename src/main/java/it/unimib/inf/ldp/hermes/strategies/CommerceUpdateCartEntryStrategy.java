package it.unimib.inf.ldp.hermes.strategies;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import it.unimib.inf.ldp.hermes.dtos.CartModification;
import it.unimib.inf.ldp.hermes.models.CartModel;
import it.unimib.inf.ldp.hermes.models.EntryModel;
import it.unimib.inf.ldp.hermes.models.ProductModel;


@Component
public class CommerceUpdateCartEntryStrategy implements UpdateCartEntryStrategy
{

    private static final Logger LOG = LogManager.getLogger();

    @Override
    public CartModification perform(EntryModel entry, long quantity)
    {

        if (entry == null)
        {
            LOG.warn("Cannot update null entry");
            return new CartModification(null, null, 0, null);
        }

        CartModel cart = entry.getOrder();
        ProductModel product = entry.getProduct();

        long newQuantity = quantity;
        if (quantity < 1)
        {
            LOG.warn("Cannot set negative quantity to product {} for cart {}. Interpreting as zero", product.getCode(),
                    cart.getCode());
            newQuantity = 0L;


            List<EntryModel> allTheEntries = cart.getEntries();
            if(allTheEntries.remove(entry))
            {
                LOG.info("deleted -> {}", allTheEntries.size());
                entry.setOrder(null);
                entry.setProduct(null);
            }
            cart.setEntries(allTheEntries);

        }

        if (newQuantity > 0)
        {
            entry.setQuantity(newQuantity);
            entry.setBasePrice(product.getPrice());
        }

        return new CartModification(product, cart, quantity, entry);
    }
}
