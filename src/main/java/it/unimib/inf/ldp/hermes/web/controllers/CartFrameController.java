package it.unimib.inf.ldp.hermes.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import it.unimib.inf.ldp.hermes.models.CartModel;
import it.unimib.inf.ldp.hermes.services.CartService;
import it.unimib.inf.ldp.hermes.services.SessionService;


@Controller
@RequestMapping("/view/cart")
public class CartFrameController
{

    @Autowired
    private CartService cartService;

    @Autowired
    private SessionService sessionService;

    @GetMapping
    public String viewCart(Model model)
    {

        CartModel cart = cartService.getSessionCart();
        model.addAttribute("cart", cart);
        model.addAttribute("session", sessionService.getCurrentSession());

        return "cart-frame";
    }


}
