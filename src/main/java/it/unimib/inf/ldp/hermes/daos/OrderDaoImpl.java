package it.unimib.inf.ldp.hermes.daos;

import org.springframework.stereotype.Repository;

import it.unimib.inf.ldp.hermes.models.OrderModel;


@Repository
public class OrderDaoImpl extends AbstractDao implements OrderDao
{

    @Override
    public OrderModel getOrderByCode(String code)
    {
        return getSession().find(OrderModel.class, code);
    }
}
