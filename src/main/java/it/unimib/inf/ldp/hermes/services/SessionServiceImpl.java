package it.unimib.inf.ldp.hermes.services;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unimib.inf.ldp.hermes.daos.SessionDao;
import it.unimib.inf.ldp.hermes.factories.HermesSessionFactory;
import it.unimib.inf.ldp.hermes.models.ClientModel;
import it.unimib.inf.ldp.hermes.models.SessionModel;
import it.unimib.inf.ldp.hermes.web.WebSession;


@Service
public class SessionServiceImpl implements SessionService
{
    private static final Logger LOG = LogManager.getLogger();

    @Autowired
    private HermesSessionFactory sessionFactory;

    @Autowired
    private SessionDao sessionDao;

    @Autowired
    private WebSession webSession;

    @Autowired
    private ModelService modelService;

    @Override
    public SessionModel retrieveSessionByID(String sessionID)
    {
        return sessionDao.findSessionByID(sessionID);
    }

    @Override
    public SessionModel createNewSession(String clientID)
    {
        SessionModel session = sessionFactory.create(clientID);
        modelService.save(session);
        return session;
    }

    @Override
    public SessionModel getCurrentSession()
    {
        SessionModel sessionModel = webSession.getSession();
        modelService.refresh(sessionModel);
        return sessionModel;
    }

    @Override
    public void refreshSession(SessionModel sessionModel)
    {
        ClientModel client = sessionModel.getClient();
        sessionFactory.refresh(sessionModel, client);
        modelService.save(sessionModel);
    }

    @Override
    public void setAttribute(String key, Object value)
    {
        if(StringUtils.isEmpty(key))
        {
            LOG.error("could not set null key");
            return;
        }
        webSession.setAttribute(key, value);
    }

    @Override
    public Object getAttribute(String key)
    {
        return webSession.getAttribute(key);
    }

}
