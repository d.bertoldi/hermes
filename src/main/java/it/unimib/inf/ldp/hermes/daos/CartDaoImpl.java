package it.unimib.inf.ldp.hermes.daos;

import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import it.unimib.inf.ldp.hermes.models.AbstractPaymentInfoModel;
import it.unimib.inf.ldp.hermes.models.CartModel;
import it.unimib.inf.ldp.hermes.models.EntryModel;
import it.unimib.inf.ldp.hermes.models.OrderModel;
import it.unimib.inf.ldp.hermes.models.OrderStatus;
import it.unimib.inf.ldp.hermes.models.PaymentModeModel;


@Repository
public class CartDaoImpl extends AbstractDao implements CartDao
{

    private static final Logger LOG = LogManager.getLogger();

    @Override
    public EntryModel findEntryById(String entryId)
    {
        try
        {
            CriteriaBuilder builder = getSession().getCriteriaBuilder();
            CriteriaQuery<EntryModel> query = builder.createQuery(EntryModel.class);
            Root<EntryModel> root = query.from(EntryModel.class);
            query.select(root).where(builder.equal(root.get(EntryModel.ID), entryId));

            Query<EntryModel> q = getSession().createQuery(query);

            List<EntryModel> results = q.getResultList();
            if (results != null && !results.isEmpty())
            {
                return results.get(0);
            }
            return null;
        }
        catch (PersistenceException pe)
        {
            return null;
        }
    }

    @Override
    public OrderModel createOrderFromCart(CartModel cart)
    {
        CartModel mergedCart = (CartModel) getSession().merge(cart);
        OrderModel order = new OrderModel();
        order.setCode("O" + mergedCart.getCode());
        order.setOrderStatus(OrderStatus.CREATED);
        order.setRelatedCart(mergedCart);
        getSession().persist(order);
        return order;
    }
}
