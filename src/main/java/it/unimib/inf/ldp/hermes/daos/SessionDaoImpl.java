package it.unimib.inf.ldp.hermes.daos;

import org.springframework.stereotype.Repository;

import it.unimib.inf.ldp.hermes.models.SessionModel;


@Repository
public class SessionDaoImpl extends AbstractDao implements SessionDao
{
    @Override
    public SessionModel create(SessionModel session)
    {
        getSession().persist(session);
        return session;
    }

    @Override
    public void delete(String sessionID)
    {
        getSession().delete(sessionID, SessionModel.class);
    }

    @Override
    public SessionModel findSessionByID(String sessionID)
    {
        return getSession().find(SessionModel.class, sessionID);
    }
}
