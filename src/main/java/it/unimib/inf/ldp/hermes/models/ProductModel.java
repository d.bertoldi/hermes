package it.unimib.inf.ldp.hermes.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "products")
public class ProductModel extends AbstractModel
{
    public static final String CODE = "code";
    public static final String NAME = "name";
    public static final String PRICE = "price";
    public static final String IMAGE = "image";

    @JsonIgnore
    @ManyToOne
    private ClientModel client;

    @Id
    @Column(name = CODE)
    private String code;

    @Column(name = NAME)
    private String name;

    @Column(name = PRICE)
    private BigDecimal price;

    @Column(name = IMAGE)
    private String image;

    public ClientModel getClient()
    {
        return client;
    }

    public void setClient(ClientModel client)
    {
        this.client = client;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public BigDecimal getPrice()
    {
        return price;
    }

    public void setPrice(BigDecimal price)
    {
        this.price = price;
    }

    public String getImage()
    {
        return image;
    }

    public void setImage(String image)
    {
        this.image = image;
    }
}
