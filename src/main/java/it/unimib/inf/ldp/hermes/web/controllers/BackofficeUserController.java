package it.unimib.inf.ldp.hermes.web.controllers;

import it.unimib.inf.ldp.hermes.services.ClientService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/createBackofficeUser")
public class BackofficeUserController
{
    @Autowired
    private ClientService clientService;

    @RequestMapping(method = RequestMethod.GET)
    public String showCreateUserForm()
    {
        return "create-backoffice-user";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String createBackofficeUser(@ModelAttribute(name = "clientID") String clientID,
            @ModelAttribute(name = "username") String username,
            @ModelAttribute(name = "password") String password, Model model)
    {
        String confirmString = "";
        String errorString = "";
        boolean updated = false;

        if (StringUtils.isEmpty(clientID))
        {
            errorString = "ClientID can't be left empty!";
        }
        else if (StringUtils.isEmpty(username))
        {
            errorString = "Username can't be left empty!";
        }

        if (errorString.equals(""))
        {
            if (StringUtils.isNotEmpty(password))
            {
                password = BCrypt.hashpw(password, BCrypt.gensalt());
            }
            updated = clientService.setClientBackofficeCredentials(clientID, username, password);
        }

        if (!updated)
        {
            errorString = "An error happen during the update, please retry.";
        }
        else
        {
            confirmString = "Credentials of Client with ID " + clientID + " updated correclty.";
        }

        model.addAttribute("confirm", confirmString);
        model.addAttribute("error", errorString);
        return "create-backoffice-user";
    }
}
