package it.unimib.inf.ldp.hermes.daos;

import it.unimib.inf.ldp.hermes.models.ClientModel;


public interface ClientDao
{

    ClientModel findClientByIDAndDomain(String clientID, String domain);

    ClientModel findClientByID(String clientID);

    ClientModel findClientByUsername(String username);

    boolean updateClientBackofficeCredentials(String clientID, String username, String passowrd);
}
