package it.unimib.inf.ldp.hermes.services;

import it.unimib.inf.ldp.hermes.models.ClientModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;

@Service
public class BackofficeSessionServiceImpl implements BackofficeSessionService
{
    private static final Logger LOG = LogManager.getLogger(BackofficeSessionServiceImpl.class);

    @Autowired
    private ClientService clientService;

    @Override
    public boolean performLogin(String username, String password, HttpServletRequest request)
    {
        ClientModel client = clientService.getClientByUsername(username);
        if (client != null && BCrypt.checkpw(password, client.getPassword()))
        {
            createSession(client, request);
            return true;
        }
        return false;
    }

    @Override
    public ClientModel performLogout(HttpServletRequest request)
    {
        HttpSession session = request.getSession(false);
        ClientModel client = (ClientModel) session.getAttribute("client");
        session.invalidate();
        return client;
    }

    @Override
    public ClientModel getClientFromSession(HttpServletRequest request)
    {
        HttpSession session = request.getSession(false);

        if (session == null || session.getAttribute("expiration") == null ||
                ((LocalDateTime) session.getAttribute("expiration")).isBefore(LocalDateTime.now()))
        {
            return null;
        }

        // refresh session expiration time
        session.setAttribute("expiration", LocalDateTime.now().plusHours(1L));

        // return client
        return ((ClientModel) session.getAttribute("client"));
    }

    private void createSession(ClientModel client, HttpServletRequest request)
    {
        HttpSession session = request.getSession(true);
        session.setAttribute("client", client);
        session.setAttribute("expiration", LocalDateTime.now().plusHours(1L));
    }
}
