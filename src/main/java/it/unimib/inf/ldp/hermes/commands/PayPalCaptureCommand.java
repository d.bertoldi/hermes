package it.unimib.inf.ldp.hermes.commands;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.braintreepayments.http.HttpResponse;
import com.braintreepayments.http.serializer.Json;
import com.paypal.orders.Order;
import com.paypal.orders.OrderRequest;
import com.paypal.orders.OrdersCaptureRequest;

import it.unimib.inf.ldp.hermes.factories.PayPalClientFactory;
import it.unimib.inf.ldp.hermes.models.CartModel;
import it.unimib.inf.ldp.hermes.models.ClientModel;
import it.unimib.inf.ldp.hermes.models.PayPalPaymentInfoModel;


@Component("paypal-capture")
public class PayPalCaptureCommand implements PaymentCommand
{
    private static final Logger LOG = LogManager.getLogger();

    @Override
    public boolean execute(CartModel cart, ClientModel client)
    {

        PayPalPaymentInfoModel paymentInfo = (PayPalPaymentInfoModel) cart.getPaymentInfo();
        OrdersCaptureRequest request = new OrdersCaptureRequest(paymentInfo.getRemoteOrderCode());
        request.requestBody(new OrderRequest());
        try
        {
            HttpResponse<Order> response = PayPalClientFactory.build(client, cart.getPaymentMode()).execute(request);
            LOG.info(new JSONObject(new Json().serialize(response.result())).toString(4));
            return "COMPLETED".equals(response.result().status());
        }
        catch (IOException e)
        {
            LOG.error("Error while contacting PayPal for order {}", cart.getCode(), e);
            return false;
        }
    }
}
