package it.unimib.inf.ldp.hermes.services;

import it.unimib.inf.ldp.hermes.models.ClientModel;

import javax.transaction.Transactional;

@Transactional
public interface ClientService
{

    ClientModel getClientByID(String clientID);

    ClientModel getClientByUsername(String username);

    boolean setClientBackofficeCredentials(String clientID, String username, String password);
}
