package it.unimib.inf.ldp.hermes.models;

import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;


@Entity
@Table(name = "payment_modes")
public class PaymentModeModel extends AbstractModel
{
    public static final String CODE = "code";

    @Id
    @Column(name = CODE)
    private String code;

    @ElementCollection(fetch = FetchType.EAGER)
    @MapKeyColumn(name="property_key")
    @Column(name="property_value")
    @CollectionTable(name="paymentmode2properties")
    private Map<String, String> properties;

    @Column
    private String providerName;

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public Map<String, String> getProperties()
    {
        return properties;
    }

    public void setProperties(Map<String, String> properties)
    {
        this.properties = properties;
    }

    public String getProviderName()
    {
        return providerName;
    }

    public void setProviderName(String providerName)
    {
        this.providerName = providerName;
    }
}
