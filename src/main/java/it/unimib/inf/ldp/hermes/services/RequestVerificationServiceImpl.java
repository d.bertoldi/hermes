package it.unimib.inf.ldp.hermes.services;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import it.unimib.inf.ldp.hermes.daos.ClientDao;
import it.unimib.inf.ldp.hermes.daos.SessionDao;
import it.unimib.inf.ldp.hermes.models.ClientModel;


@Service
public class RequestVerificationServiceImpl implements ClientVerificationService
{
    private static final Logger LOG = LogManager.getLogger();

    @Autowired
    private ClientDao clientDao;

    @Autowired
    private SessionDao sessionDao;

    @Override
    public boolean isVerified(String clientID, String domain)
    {
        ClientModel client = clientDao.findClientByIDAndDomain(clientID, domain);
        return client != null;
    }

    public boolean isVerified(String sessionID)
    {
        return sessionDao.findSessionByID(sessionID) != null;
    }

    @Override
    public Result isVerified(HttpServletRequest request)
    {
        String clientID = request.getParameter("clientID");
        if(StringUtils.isEmpty(clientID))
        {
            LOG.info("clientID not set for request {}", request::getPathInfo);
            return Result.INVALID_CLIENT;
        }
        String domain = request.getRemoteAddr();
        return isVerified(clientID, domain) ? Result.OK : Result.INVALID_CLIENT_DOMAIN;
    }

}
