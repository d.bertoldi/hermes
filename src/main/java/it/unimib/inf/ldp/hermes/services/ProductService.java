package it.unimib.inf.ldp.hermes.services;

import it.unimib.inf.ldp.hermes.models.ProductModel;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface ProductService
{
    List<ProductModel> getAllProductsForClientID(String clientID);

    ProductModel getProductFromCodeAndClientID(String productCode, String clientID);

    void createProduct(ProductModel product);

    void updateProduct(ProductModel product);

    void deleteProduct(String productCode, String clientID);
}
