package it.unimib.inf.ldp.hermes.web.interceptors;

import java.util.Arrays;
import java.util.Properties;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import it.unimib.inf.ldp.hermes.models.SessionModel;
import it.unimib.inf.ldp.hermes.services.ClientVerificationService;
import it.unimib.inf.ldp.hermes.services.SessionService;


public class SessionInterceptor extends HandlerInterceptorAdapter
{

    private static final Logger LOG = LogManager.getLogger();

    private static final String SESSION_COOKIE_NAME_PREFIX = "hSession4";

    private static final String IGNORED_RESOURCES = "ignored.resources";

    @Autowired
    protected ClientVerificationService clientVerificationService;

    @Autowired
    protected SessionService sessionService;

    @Autowired
    protected Properties properties;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
    {
        String path = request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE) != null ?
                ((String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE)) :
                request.getRequestURI();
        if (Arrays.stream(properties.getProperty(IGNORED_RESOURCES, "").split(","))
                .anyMatch(r -> StringUtils.startsWith(path, r.trim())))
        {
            LOG.info("Request {} is ignored", request.getRequestURI());
            return true;
        }

        boolean isSessionForced = isSessionForced(request);
        String sessionID = getSessionID(request, isSessionForced);
        ClientVerificationService.Result result = clientVerificationService.isVerified(request);

        if (StringUtils.isEmpty(sessionID))
        {
            LOG.info("Invalid cookie for session");

            switch (result)
            {
                case OK:
                    LOG.info("Client and domain verified: creating and binding new session");
                    SessionModel sessionModel = sessionService.createNewSession(getClientID(request));
                    bindSession(request, sessionModel);
                    setSessionCookie(request, response, sessionModel);
                    return true;
                case INVALID_CLIENT:
                    LOG.warn("Invalid client {}", getClientID(request));
                    return false;

                case INVALID_CLIENT_DOMAIN:
                    LOG.warn("Invalid domain {}", request.getRemoteAddr());
                    return false;
                default:
                    LOG.error("Verification response not handled: {}", result.name());
                    return false;
            }

        }
        else
        {
            SessionModel sessionModel = sessionService.retrieveSessionByID(sessionID);
            if (sessionModel != null)
            {
                LOG.info("Session {} is valid", sessionID);
                sessionService.refreshSession(sessionModel);
                bindSession(request, isSessionForced);
                return true;
            }
            LOG.warn("Session {} is NOT valid, recreating...", sessionID);
            LOG.info("Client and domain verified: creating and binding new session");
            sessionModel = sessionService.createNewSession(getClientID(request));
            bindSession(request, sessionModel);
            setSessionCookie(request, response, sessionModel);
            return true;
        }
    }

    private static String getSessionID(HttpServletRequest request, boolean forced)
    {
        if (forced)
        {
            LOG.info("Session is forced");
            return request.getParameter("forcedSession");
        }

        Cookie cookie = WebUtils.getCookie(request, getCookieName(request));
        if (cookie == null)
        {
            return null;
        }
        return cookie.getValue();
    }

    private static boolean isSessionForced(HttpServletRequest request)
    {
        return StringUtils.isNotEmpty(request.getParameter("forcedSession"));

    }

    private static String getClientID(HttpServletRequest request)
    {
        return request.getParameter("clientID");
    }

    private void bindSession(HttpServletRequest request, boolean isSessionForced)
    {
        String sessionID = getSessionID(request, isSessionForced);
        SessionModel sessionModel = sessionService.retrieveSessionByID(sessionID);
        if (sessionModel != null)
        {
            bindSession(request, sessionModel);
        }
    }

    private void bindSession(HttpServletRequest request, SessionModel sessionModel)
    {
        HttpSession httpSession = request.getSession();
        httpSession.setAttribute(getCookieName(request), sessionModel);

    }

    private static void setSessionCookie(HttpServletRequest request, HttpServletResponse response, SessionModel sessionModel)
    {
        Cookie sessionCookie = new Cookie(getCookieName(request), sessionModel.getId());
        sessionCookie.setMaxAge(Integer.MAX_VALUE);
        sessionCookie.setPath("/");
        response.addCookie(sessionCookie);
    }

    private static String getCookieName(HttpServletRequest request)
    {
        return SESSION_COOKIE_NAME_PREFIX + getClientID(request);
    }

}
