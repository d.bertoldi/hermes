package it.unimib.inf.ldp.hermes.dtos;

import java.io.Serializable;


public class CartActionResponse implements Serializable
{

    public long newQuantity;

    public CartActionResponse()
    {
    }

    public CartActionResponse(long newQuantity)
    {
        this.newQuantity = newQuantity;
    }

    public long getNewQuantity()
    {
        return newQuantity;
    }

    public void setNewQuantity(long newQuantity)
    {
        this.newQuantity = newQuantity;
    }
}
