package it.unimib.inf.ldp.hermes.daos;

import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import it.unimib.inf.ldp.hermes.models.ClientModel;


@Repository
public class ClientDaoImpl extends AbstractDao implements ClientDao
{

    @Override
    public ClientModel findClientByIDAndDomain(String clientID, String domain)
    {
        try
        {
            CriteriaBuilder builder = getSession().getCriteriaBuilder();
            CriteriaQuery<ClientModel> query = builder.createQuery(ClientModel.class);
            Root<ClientModel> root = query.from(ClientModel.class);
            query.select(root).where(builder
                    .and(builder.equal(root.get(ClientModel.ID), clientID), builder.equal(root.get(ClientModel.DOMAIN), domain)));

            Query<ClientModel> q = getSession().createQuery(query);
            List<ClientModel> results = q.getResultList();
            if (results != null && !results.isEmpty())
            {
                return results.get(0);
            }
            return null;
        }
        catch (PersistenceException pe)
        {
            return null;
        }
    }

    @Override
    public ClientModel findClientByID(String clientID)
    {
        try
        {
            CriteriaBuilder builder = getSession().getCriteriaBuilder();
            CriteriaQuery<ClientModel> query = builder.createQuery(ClientModel.class);
            Root<ClientModel> root = query.from(ClientModel.class);
            query.select(root).where(builder.equal(root.get(ClientModel.ID), clientID));

            Query<ClientModel> q = getSession().createQuery(query);
            List<ClientModel> results = q.getResultList();
            if (results != null && !results.isEmpty())
            {
                return results.get(0);
            }
            return null;
        }
        catch (PersistenceException pe)
        {
            return null;
        }
    }

    @Override
    public ClientModel findClientByUsername(String username)
    {
        try
        {
            CriteriaBuilder builder = getSession().getCriteriaBuilder();
            CriteriaQuery<ClientModel> query = builder.createQuery(ClientModel.class);
            Root<ClientModel> root = query.from(ClientModel.class);
            query.select(root).where(builder.equal(root.get(ClientModel.USERNAME), username));

            Query<ClientModel> q = getSession().createQuery(query);
            List<ClientModel> results = q.getResultList();
            if (results != null && !results.isEmpty()) {
                return results.get(0);
            }
            return null;
        }
        catch(PersistenceException pe)
        {
            return null;
        }
    }

    @Override
    public boolean updateClientBackofficeCredentials(String clientID, String username, String passowrd)
    {
        try
        {
            CriteriaBuilder builder = getSession().getCriteriaBuilder();
            CriteriaUpdate<ClientModel> updateQuery = builder.createCriteriaUpdate(ClientModel.class);
            Root<ClientModel> root = updateQuery.from(ClientModel.class);
            updateQuery.set(ClientModel.USERNAME, username);
            updateQuery.set(ClientModel.PASSWORD, passowrd);
            updateQuery.where(builder.equal(root.get(ClientModel.ID), clientID));

            int updated = getSession().createQuery(updateQuery).executeUpdate();
            if (updated > 0)
            {
                return true;
            }
        }
        catch(PersistenceException pe)
        {
            return false;
        }

        return false;
    }
}
