package it.unimib.inf.ldp.hermes.exceptions;

public class ProductNotFoundException extends RuntimeException
{
    public ProductNotFoundException(String message)
    {
        super(message);
    }

    public ProductNotFoundException()
    {
        super();
    }
}
