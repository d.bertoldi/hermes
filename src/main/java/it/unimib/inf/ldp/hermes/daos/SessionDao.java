package it.unimib.inf.ldp.hermes.daos;

import it.unimib.inf.ldp.hermes.models.SessionModel;


public interface SessionDao
{

    SessionModel create(SessionModel session);

    void delete(String sessionID);

    SessionModel findSessionByID(String sessionID);


}
