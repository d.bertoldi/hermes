package it.unimib.inf.ldp.hermes.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unimib.inf.ldp.hermes.daos.ClientDao;
import it.unimib.inf.ldp.hermes.models.ClientModel;

@Service
public class ClientServiceImpl implements ClientService
{
    @Autowired
    private ClientDao clientDao;

    @Override
    public ClientModel getClientByID(String clientID)
    {
        return clientDao.findClientByID(clientID);
    }

    @Override
    public ClientModel getClientByUsername(String username)
    {
        return clientDao.findClientByUsername(username);
    }

    @Override
    public boolean setClientBackofficeCredentials(String clientID, String username, String password)
    {
        return clientDao.updateClientBackofficeCredentials(clientID, username, password);
    }

}
