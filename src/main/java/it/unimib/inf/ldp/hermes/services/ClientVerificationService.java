package it.unimib.inf.ldp.hermes.services;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;


@Transactional
public interface ClientVerificationService
{

    boolean isVerified(String clientID, String domain);

    boolean isVerified(String sessionID);

    Result isVerified(HttpServletRequest request);

    enum Result {
        INVALID_CLIENT_DOMAIN, INVALID_CLIENT, OK;
    }
}
