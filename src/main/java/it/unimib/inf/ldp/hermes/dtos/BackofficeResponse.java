package it.unimib.inf.ldp.hermes.dtos;

public class BackofficeResponse
{
    private Result result;
    private String errorText;
    private String redirectUrl;

    public BackofficeResponse(Result result, String errorText, String redirectUrl)
    {
        this.result = result;
        this.errorText = errorText;
        this.redirectUrl = redirectUrl;
    }

    public BackofficeResponse()
    {

    }

    public Result getResult()
    {
        return result;
    }

    public void setResult(Result result)
    {
        this.result = result;
    }

    public String getErrorText()
    {
        return errorText;
    }

    public void setErrorText(String errorText)
    {
        this.errorText = errorText;
    }

    public String getRedirectUrl()
    {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl)
    {
        this.redirectUrl = redirectUrl;
    }

    public enum Result
    {
        OK("OK"),
        NOK("NOK");

        private String value;

        Result(String value)
        {
            this.value = value;
        }
    };
}
