package it.unimib.inf.ldp.hermes.factories;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unimib.inf.ldp.hermes.daos.ClientDao;
import it.unimib.inf.ldp.hermes.models.ClientModel;
import it.unimib.inf.ldp.hermes.models.SessionModel;
import it.unimib.inf.ldp.hermes.web.WebSession;


@Service
public class HermesSessionFactory
{

    private static final long DURATION = 60L;

    @Autowired
    private WebSession webSession;

    @Resource
    private ClientDao clientDao;

    public SessionModel create(String clientID)
    {
        ClientModel client = clientDao.findClientByID(clientID);
        SessionModel session = new SessionModel(generateID());
        return refresh(session, client);

    }

    public SessionModel refresh(SessionModel session, ClientModel client)
    {
        session.setExpirationTime(LocalDateTime.now().plusSeconds(DURATION));
        session.setClient(client);
        webSession.setSession(session);

        webSession.setAttribute("currency", client.getCurrency());

        return session;
    }

    protected String generateID()
    {
        return UUID.randomUUID().toString();
    }

}
