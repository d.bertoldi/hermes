package it.unimib.inf.ldp.hermes.dtos;

import java.io.Serializable;


public class CheckoutRequest implements Serializable
{

    private String orderCode;

    public String getOrderCode()
    {
        return orderCode;
    }

    public void setOrderCode(String orderCode)
    {
        this.orderCode = orderCode;
    }
}
