package it.unimib.inf.ldp.hermes.strategies;

import it.unimib.inf.ldp.hermes.dtos.CartModification;
import it.unimib.inf.ldp.hermes.models.CartModel;
import it.unimib.inf.ldp.hermes.models.ProductModel;


public interface AddToCartStrategy
{
    CartModification perform(CartModel cart, ProductModel product, long quantity);


}
