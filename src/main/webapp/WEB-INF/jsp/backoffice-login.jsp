<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Backoffice Login Page</title>
</head>
<body>

    <form id="loginForm" method="post">
        <label for="loginUsername">Username:</label>
        <input type="text" name="username" id="loginUsername"/><br/>
        <label for="loginPassword">Password:</label>
        <input type="password" name="password" id="loginPassword"/><br/>
        <input type="submit" value="Login" />
    </form>

</body>
</html>
