<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Create Backoffice User Page</title>
</head>
<body>

<c:if test="${not empty confirm}" >
    <p style="cont-style: italic; font-weight: bold;">${confirm}</p>
</c:if>
<c:if test="${not empty error}" >
    <p style="font-style: italic; font-weight: bold; color: red;">${error}</p>
</c:if>
    <form id="createBackofficeUser" method="post">
        <label for="clientID">Client ID:</label><input type="text" name="clientID" id="clientID" /><br/>
        <label for="username">Username:</label><input type="text" name="username" id="username" /><br/>
        <label for="password">Password:</label><input type="password" name="password" id="password" /><br/>
        <input type="submit" value="Create user" />
    </form>

</body>
</html>
