<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="options" value="cache: false,dataType: 'jsonp', async: true"/>

window._bc = function(element){
        $.ajax({url: 'http://hermes.local:8080/hermes?clientID=${clientID}' ,cache: false, headers: {'Access-Control-Allow-Origin': '*'}, });};

window._atc = function(element){
        var code=element.dataset.productcode;
        var name=element.dataset.productname;
        var img=element.dataset.productimg;
        var price=element.dataset.productprice;
        var quantity=element.dataset.quantity;
        $.ajax({url: 'http://hermes.local:8080/hermes/action/cart/add?clientID=${clientID}&productCode='+code+'&productName='+name+'&productImg='+img+'&productPrice='+price+'&quantity='+quantity ,${options}});};

window._cc = function(element){
        $.ajax({url: 'http://hermes.local:8080/hermes/action/cart/clear?clientID=${clientID}',${options}});
};

$(document).ready(function() {
        $("div[data-hermes='cartview']").append("<iframe src='http://hermes.local:8080/hermes/view/cart?clientID=${clientID}' frameBorder='0' style='width: 100%; height: 80%'></iframe>");
        $("div[data-hermes='checkoutview']").append("<iframe src='http://hermes.local:8080/hermes/view/checkout?clientID=${clientID}' frameBorder='0' style='width: 100%; height: 80%'></iframe>");
        });