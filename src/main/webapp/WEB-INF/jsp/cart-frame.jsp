<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

  <script>
    window._ue = function (element) {
      var id = element.dataset.entryid;
      var q = element.dataset.quantity;
      $.ajax({
        url: 'http://localhost:8080/hermes/action/cart/update?clientID=${param.clientID}&forcedSession=${session.id}&entryId=' + id + '&diff=' + q,
        cache: false,
        async: false,
        dataType: 'jsonp',
        success: function (data) {
          console.log("new quantity: " + data.newQuantity);
          document.location.reload(true);
        }
      });
    };

  </script>
</head>

${cart.code}

<table>
  <c:forEach items="${cart.entries}" var="entry">
    <tr>
      <td><img src="${entry.product.image}" style="width: 64px; height: 64px"/></td>
      <td>${entry.product.name}</td>
      <td>x${entry.quantity}</td>
      <td><a href="#" onclick="_ue(this);" data-entryid="${entry.id}" data-quantity="1">+</a></td>
      <td><a href="#" onclick="_ue(this);" data-entryid="${entry.id}" data-quantity="-1">-</a></td>
      <td>${entry.totalPrice}${cart.currency.symbol}</td>
    </tr>
  </c:forEach>
</table>
Costo spedizioni: ${cart.deliveryCost}<br/>
Totale: ${cart.totalPrice} ${cart.currency.symbol}

</html>