<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
</head>

<body>
  <script src="https://www.paypal.com/sdk/js?client-id=${ppClientId}&currency=${cart.currency.toString()}">
  </script>

  <table>
    <c:forEach items="${cart.entries}" var="entry">
      <tr>
        <td><img src="${entry.product.image}" style="width: 64px; height: 64px"/></td>
        <td>${entry.product.name}</td>
        <td>x${entry.quantity}</td>
        <td>${entry.totalPrice}${cart.currency.symbol}</td>
      </tr>
    </c:forEach>
  </table>
  Costo spedizioni: ${cart.deliveryCost}<br/>
  Totale: ${cart.totalPrice} ${cart.currency.symbol}



  <div id="paypal-button-container"></div>

  <script>
    paypal.Buttons({
      createOrder: function(data, actions) {
        return actions.order.create({
          purchase_units: [{
            amount: {
              value: '${cart.totalPrice.doubleValue()}'
            }
          }]
        });
      },
      onApprove: function(data, actions) {
        return actions.order.capture().then(function(details) {

          return fetch('/hermes/action/checkout/paypal/verify?clientID=${session.client.id}', {
            credentials: 'include',
            method: 'post',
            headers: {
              'content-type': 'application/json'
            },
            body: JSON.stringify({
              orderCode: data.orderID
            })
          }).then(function (response) {

            return response.text().then(function (text) {

              if(text === "OK")
              {
                document.getElementById("paypal-button-container").insertAdjacentHTML("afterend", "<a href='/hermes/view/checkout/placeOrder?clientID=${session.client.id}'>PLACE ORDER!!!!</a>");

              }else
              {
                alert("Errore :(" + response);
              }
            });


          });
        });
      }
    }).render('#paypal-button-container');
  </script>


</body>
</html>