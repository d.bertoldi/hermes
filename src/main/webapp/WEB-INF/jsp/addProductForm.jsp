<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Create product</title>
    <script>
        function submitForm (event)
        {
            // prevent form submit
            event.preventDefault();

            var req = "";
            var productForm = document.getElementById("productForm");

            Array.from(productForm.getElementsByTagName("input")).forEach(
                function (element)
                {
                    if (element.type == "submit")
                        return;
                    if (req != "")
                    {
                        req += "&";
                    }
                    req += element.name + "=" + element.value;
                }
            );

            console.log("Request params: " + req);

            var request = new XMLHttpRequest();
            /*request.onreadystatechange = function ()
            {
                if (request.readyState === 4)
                {
                    request.response
                }
            };*/
            request.open("POST", productForm.action, true);
            request.setRequestHeader("Content-type","application/x-www-form-urlencoded");
            request.send(req);
        }
    </script>
</head>
<body>
    <form id="productForm" method="post" action="${originUrl}/backoffice/${action}" onsubmit="submitForm(event);">
        <label for="productCode">Code:</label><input type="text" name="code" id="productCode" <c:if test="${updateProduct}">value="${productCode}" disabled</c:if> /><br/>
        <label for="productName">Name:</label><input type="text" name="name" id="productName" /><br/>
        <label for="productPrice">Price:</label><input type="text" name="price" id="productPrice" /><br/>
        <label for="productImage">Image url:</label><input type="text" name="image" id="productImage" /><br/>
        <input type="submit" value="Create product">
    </form>
</body>
</html>
