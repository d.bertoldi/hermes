<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
</head>

<body>

<c:set var="cart" value="${order.relatedCart}"/>
<h2>Grazie</h2>
<h3>Ordine: ${cart.code}</h3>

<table>
  <c:forEach items="${cart.entries}" var="entry">
    <tr>
      <td><img src="${entry.product.image}" style="width: 64px; height: 64px"/></td>
      <td>${entry.product.name}</td>
      <td>x${entry.quantity}</td>
      <td>${entry.totalPrice}${cart.currency.symbol}</td>
    </tr>
  </c:forEach>
</table>
Costo spedizioni: ${cart.deliveryCost}<br/>
Totale: ${cart.totalPrice} ${cart.currency.symbol}
<br/><br/>
Indirizzo di spedizione:<br/>
${cart.deliveryAddress.firstName} ${cart.deliveryAddress.lastName}<br/>
${cart.deliveryAddress.street}<br/>
${cart.deliveryAddress.city}<br/>
${cart.deliveryAddress.zipcode}<br/>
${cart.deliveryAddress.country}

</body>
</html>