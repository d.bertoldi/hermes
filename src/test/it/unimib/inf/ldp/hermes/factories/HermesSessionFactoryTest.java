package it.unimib.inf.ldp.hermes.factories;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import java.util.Currency;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import it.unimib.inf.ldp.hermes.daos.ClientDao;
import it.unimib.inf.ldp.hermes.models.ClientModel;
import it.unimib.inf.ldp.hermes.models.SessionModel;
import it.unimib.inf.ldp.hermes.web.WebSession;


public class HermesSessionFactoryTest
{
    private static final String CLIENT_ID = "client id";

    private static final String UUID = "random";

    private static final Currency USD = Currency.getInstance("USD");

    @Spy
    @InjectMocks
    private HermesSessionFactory classUnderTest;

    @Mock
    private ClientDao clientDao;

    @Mock
    private ClientModel client;

    @Mock
    private WebSession webSession;

    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);

        given(clientDao.findClientByID(CLIENT_ID)).willReturn(client);
        given(client.getCurrency()).willReturn(USD);
    }

    @Test
    public void testUUID()
    {
        // GIVEN

        // WHEN
        String uuid = classUnderTest.generateID();
        String uuid2 = classUnderTest.generateID();

        // THEN
        assertTrue(StringUtils.isNotEmpty(uuid));
        assertFalse(StringUtils.equals(uuid, uuid2));
    }

    @Test
    public void testCreate()
    {
        // GIVEN
        given(classUnderTest.generateID()).willReturn(UUID);

        // WHEN
        SessionModel session = classUnderTest.create(CLIENT_ID);

        // THEN
        assertNotNull(session);
        assertEquals(client, session.getClient());
        assertEquals(session.getId(), UUID);
    }

    @Test
    public void testRefresh()
    {
        // GIVEN
        SessionModel session = new SessionModel(UUID);

        // WHEN
        session = classUnderTest.refresh(session, client);

        // THEN
        assertNotNull(session);
        verify(webSession).setAttribute("currency", USD);
        verify(webSession).setSession(session);
    }
}
