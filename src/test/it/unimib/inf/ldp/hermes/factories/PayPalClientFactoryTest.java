package it.unimib.inf.ldp.hermes.factories;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.paypal.core.PayPalEnvironment;
import com.paypal.core.PayPalHttpClient;

import it.unimib.inf.ldp.hermes.models.ClientModel;
import it.unimib.inf.ldp.hermes.models.PaymentModeModel;


public class PayPalClientFactoryTest
{

    private ClientModel client;

    private PaymentModeModel paymentMode;

    @Before
    public void setup()
    {
        client = new ClientModel();
        client.setId("123");

        paymentMode = new PaymentModeModel();
        Map<String, String> properties = new HashMap<>();
        properties.put(PayPalClientFactory.CLIENT_ID_KEY, "client id");
        properties.put(PayPalClientFactory.CLIENT_SECRET_KEY, "client secret");
        paymentMode.setProperties(properties);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullPaymentMode()
    {
        // GIVEN

        // WHEN
        PayPalClientFactory.build(client, null);

        // THEN

    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullProperties()
    {
        // GIVEN
        paymentMode.setProperties(null);

        // WHEN
        PayPalClientFactory.build(client, paymentMode);

        // THEN

    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullClient()
    {
        // GIVEN

        // WHEN
        PayPalClientFactory.build(null, paymentMode);

        // THEN

    }

    @Test(expected = IllegalStateException.class)
    public void testEmptyProperties()
    {
        // GIVEN
        Map<String, String> properties = new HashMap<>();
        properties.put(PayPalClientFactory.CLIENT_ID_KEY, "client id value");
        paymentMode.setProperties(properties);

        // WHEN
        PayPalClientFactory.build(client, paymentMode);

        // THEN
    }
    @Test(expected = IllegalStateException.class)
    public void testEmptyProperties2()
    {
        // GIVEN
        Map<String, String> properties = new HashMap<>();
        properties.put(PayPalClientFactory.CLIENT_SECRET_KEY, "client secret value");
        paymentMode.setProperties(properties);

        // WHEN
        PayPalClientFactory.build(client, paymentMode);

        // THEN
    }

    @Test
    public void testSandbox()
    {
        // GIVEN
        client.setProductionEnvironment(false);

        // WHEN
        PayPalEnvironment env = PayPalClientFactory.getEnvironment(client, "", "");

        // THEN
        assertTrue(env instanceof PayPalEnvironment.Sandbox);
    }

    @Test
    public void testProduction()
    {
        // GIVEN
        client.setProductionEnvironment(true);

        // WHEN
        PayPalEnvironment env = PayPalClientFactory.getEnvironment(client, "", "");

        // THEN
        assertTrue(env instanceof PayPalEnvironment.Live);
    }

    @Test
    public void testBuild()
    {
        // GIVEN
        client.setProductionEnvironment(true);

        // WHEN
        PayPalHttpClient ppClient = PayPalClientFactory.build(client, paymentMode);

        // THEN
        assertNotNull(ppClient);
    }

}
