package it.unimib.inf.ldp.hermes.services;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.Currency;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import it.unimib.inf.ldp.hermes.daos.CartDao;
import it.unimib.inf.ldp.hermes.dtos.CartModification;
import it.unimib.inf.ldp.hermes.models.CartModel;
import it.unimib.inf.ldp.hermes.models.ClientModel;
import it.unimib.inf.ldp.hermes.models.EntryModel;
import it.unimib.inf.ldp.hermes.models.PaymentModeModel;
import it.unimib.inf.ldp.hermes.models.ProductModel;
import it.unimib.inf.ldp.hermes.models.SessionModel;
import it.unimib.inf.ldp.hermes.strategies.AddToCartStrategy;
import it.unimib.inf.ldp.hermes.strategies.UpdateCartEntryStrategy;


public class CartServiceImplTest
{
    private static final String PAYMENT_MODE_ID = "mode-id";

    private static final String SESSION_ID = "session id";

    private static final String CLIENT_MODE = "mode id 4 client";

    private static final String CLIENT_ID = "client-id";

    private static final Currency USD = Currency.getInstance("USD");

    @Spy
    @InjectMocks
    private CartServiceImpl classUnderTest;

    @Mock
    private SessionService sessionService;

    @Mock
    private ModelService modelService;

    @Mock
    private AddToCartStrategy addToCartStrategy;

    @Mock
    private UpdateCartEntryStrategy updateCartEntryStrategy;

    @Mock
    private CalculationService calculationService;

    @Mock
    private CartDao cartDao;

    private CartModel cart;

    private SessionModel session;

    private ClientModel client;

    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);

        cart = new CartModel();
        cart.setCode("code");
        session = new SessionModel(SESSION_ID);
        client = new ClientModel();
        client.setId(CLIENT_ID);
        session.setClient(client);
        session.setCart(cart);

        PaymentModeModel p1 = new PaymentModeModel();
        p1.setCode("p1");
        PaymentModeModel p2 = new PaymentModeModel();
        p2.setCode("p2");
        PaymentModeModel p3 = new PaymentModeModel();
        p3.setCode(CLIENT_MODE);
        client.setSupportedPaymentModes(Arrays.asList(p1, p2, p3));

        doReturn(cart).when(classUnderTest).getSessionCart();
        given(sessionService.getCurrentSession()).willReturn(session);
        doReturn(CLIENT_MODE).when(classUnderTest).getPaymentModeId(PAYMENT_MODE_ID, client);
    }

    @Test
    public void testNoCart()
    {
        // GIVEN
        doReturn(null).when(classUnderTest).getSessionCart();

        // WHEN
        classUnderTest.setPaymentMode(PAYMENT_MODE_ID);

        // THEN
        verify(classUnderTest, never()).getPaymentModeId(anyString(), any(ClientModel.class));
        verify(modelService, never()).save(any(CartModel.class));
    }

    @Test
    public void testNotSupported()
    {
        // GIVEN
        PaymentModeModel p1 = new PaymentModeModel();
        p1.setCode("p1");
        PaymentModeModel p2 = new PaymentModeModel();
        p2.setCode("p2");
        client.setSupportedPaymentModes(Arrays.asList(p1, p2));

        // WHEN
        classUnderTest.setPaymentMode(PAYMENT_MODE_ID);

        // THEN
        verify(modelService, never()).save(cart);
    }

    @Test
    public void testSupported()
    {
        // GIVEN

        // WHEN
        classUnderTest.setPaymentMode(PAYMENT_MODE_ID);

        // THEN
        assertEquals(session.getCart(), cart);
        assertEquals(client.getSupportedPaymentModes().get(2), cart.getPaymentMode());
        verify(modelService).save(cart);
        verify(modelService).save(session);
    }

    @Test
    public void testProduct()
    {
        // GIVEN
        String code = "code";
        String name = "name";
        String image = "image";
        double price = 12.23;

        // WHEN
        ProductModel product = classUnderTest.toProduct(code, name, image, price);

        // THEN
        assertEquals(client, product.getClient());
        assertEquals(code, product.getCode());
        assertEquals(name, product.getName());
        assertEquals(image, product.getImage());
        assertEquals(price, product.getPrice().doubleValue());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testProductNoCode()
    {
        // GIVEN
        String code = "";
        String name = "name";
        String image = "image";
        double price = 12.23;

        // WHEN
        classUnderTest.toProduct(code, name, image, price);

        // THEN

    }

    @Test(expected = IllegalArgumentException.class)
    public void testProducInvalidPrice()
    {
        // GIVEN
        String code = "code";
        String name = "name";
        String image = "image";
        double price = -0.01;

        // WHEN
        classUnderTest.toProduct(code, name, image, price);

        // THEN
    }

    @Test
    public void testSessionCart()
    {
        // GIVEN
        Mockito.reset(classUnderTest);

        // WHEN
        CartModel result = classUnderTest.getSessionCart();

        // THEN
        assertEquals(cart, result);
    }

    @Test
    public void testNoSessionCart()
    {
        // GIVEN
        Mockito.reset(classUnderTest);
        session.setCart(null);

        // WHEN
        CartModel result = classUnderTest.getSessionCart();

        // THEN
        assertNull(result);
    }

    @Test
    public void testCreateCart()
    {
        // GIVEN
        given(sessionService.getAttribute("currency")).willReturn(USD);

        // WHEN
        CartModel result = classUnderTest.createCart(session);

        // THEN
        assertTrue(Pattern.matches(CLIENT_ID + "[0-9]*-[A-Za-z0-9]*", result.getCode()));
        assertEquals(USD, result.getCurrency());
        assertEquals(client, result.getClient());
        verify(modelService).save(any(CartModel.class));
    }

    @Test
    public void testGetId()
    {
        // GIVEN
        Mockito.reset(classUnderTest);

        // WHEN
        String result = classUnderTest.getPaymentModeId(PAYMENT_MODE_ID, client);

        // THEN
        assertEquals(PAYMENT_MODE_ID + "4" + client.getId(), result);
    }

    @Test
    public void testAddToCartNoCart()
    {
        // GIVEN
        Mockito.reset(classUnderTest);
        session.setCart(null);
        ProductModel p = new ProductModel();

        given(addToCartStrategy.perform(cart, p, 5)).willReturn(new CartModification(p, cart, 5, new EntryModel()));
        doNothing().when(calculationService).calculate(cart, true);

        // WHEN
        classUnderTest.addToCart(p, 5);

        // THEN
        verify(classUnderTest).createCart(session);
    }

    @Test
    public void testAddToCartWithCart()
    {
        // GIVEN
        Mockito.reset(classUnderTest);
        ProductModel p = new ProductModel();

        given(addToCartStrategy.perform(cart, p, 5)).willReturn(new CartModification(p, cart, 5, new EntryModel()));
        doNothing().when(calculationService).calculate(cart, true);

        // WHEN
        classUnderTest.addToCart(p, 5);

        // THEN
        verify(classUnderTest, never()).createCart(session);
    }

    @Test
    public void testUpdateEntryNoEntry()
    {
        // GIVEN
        String entryID = "entryID";
        given(cartDao.findEntryById(entryID)).willReturn(null);

        // WHEN
        CartModification result = classUnderTest.updateEntry(entryID, 5);

        // THEN
        assertNull(result);
    }

    @Test
    public void testUpdateEntry()
    {
        // GIVEN
        String entryID = "entryID";
        EntryModel entry = new EntryModel();
        doReturn(entry).when(classUnderTest).getEntryById(entryID);

        given(updateCartEntryStrategy.perform(entry, 5)).willReturn(new CartModification(new ProductModel(), cart, 7, entry));

        // WHEN
        CartModification result = classUnderTest.updateEntry(entryID, 5);

        // THEN
        assertNotNull(result);
        verify(calculationService).calculate(cart, true);
        verify(modelService, never()).remove(entry);
    }

    @Test
    public void testUpdateEntryRemove()
    {
        // GIVEN
        String entryID = "entryID";
        EntryModel entry = new EntryModel();
        doReturn(entry).when(classUnderTest).getEntryById(entryID);

        given(updateCartEntryStrategy.perform(entry, 5)).willReturn(new CartModification(new ProductModel(), cart, 0, entry));

        // WHEN
        CartModification result = classUnderTest.updateEntry(entryID, 5);

        // THEN
        assertNotNull(result);
        verify(calculationService).calculate(cart, true);
        verify(modelService).remove(entry);

    }


    @Test(expected = IllegalStateException.class)
    public void testUpdateEntryInvalid()
    {
        // GIVEN
        String entryID = "entryID";
        EntryModel entry = new EntryModel();
        doReturn(entry).when(classUnderTest).getEntryById(entryID);

        given(updateCartEntryStrategy.perform(entry, 5)).willReturn(new CartModification(new ProductModel(), cart, -10, entry));

        // WHEN
        classUnderTest.updateEntry(entryID, 5);

        // THEN

    }

    @Test(expected = IllegalStateException.class)
    public void testUpdateEntryInvalid2()
    {
        // GIVEN
        String entryID = "entryID";
        EntryModel entry = new EntryModel();
        doReturn(entry).when(classUnderTest).getEntryById(entryID);

        given(updateCartEntryStrategy.perform(entry, 5)).willReturn(new CartModification(new ProductModel(), cart, 10, null));

        // WHEN
        classUnderTest.updateEntry(entryID, 5);

        // THEN

    }

    @Test(expected = IllegalStateException.class)
    public void testUpdateEntryInvalid3()
    {
        // GIVEN
        String entryID = "entryID";
        EntryModel entry = new EntryModel();
        doReturn(entry).when(classUnderTest).getEntryById(entryID);

        given(updateCartEntryStrategy.perform(entry, 5)).willReturn(new CartModification(new ProductModel(), cart, 0, null));

        // WHEN
        classUnderTest.updateEntry(entryID, 5);

        // THEN

    }

    @Test
    public void testClearCartNoCart()
    {
        // GIVEN
        session.setCart(null);

        // WHEN
        classUnderTest.clearCart();

        // THEN
        verify(modelService, never()).save(any());
        verify(modelService, never()).remove(any());


    }


    @Test
    public void testClearCart()
    {
        // GIVEN

        // WHEN
        classUnderTest.clearCart();

        // THEN
        verify(modelService).save(any(SessionModel.class));
        verify(modelService).remove(any(CartModel.class));
        assertNull(cart.getPaymentInfo());
    }
}
