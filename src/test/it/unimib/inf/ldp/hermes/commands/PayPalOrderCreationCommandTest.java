package it.unimib.inf.ldp.hermes.commands;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collections;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.braintreepayments.http.HttpResponse;
import com.paypal.core.PayPalHttpClient;
import com.paypal.orders.AddressPortable;
import com.paypal.orders.AmountWithBreakdown;
import com.paypal.orders.Customer;
import com.paypal.orders.Name;
import com.paypal.orders.Order;
import com.paypal.orders.OrdersGetRequest;
import com.paypal.orders.PurchaseUnit;
import com.paypal.orders.ShippingDetails;

import it.unimib.inf.ldp.hermes.factories.PayPalClientFactory;
import it.unimib.inf.ldp.hermes.models.AddressModel;
import it.unimib.inf.ldp.hermes.models.CartModel;
import it.unimib.inf.ldp.hermes.models.ClientModel;
import it.unimib.inf.ldp.hermes.models.PayPalPaymentInfoModel;
import it.unimib.inf.ldp.hermes.models.PaymentModeModel;
import it.unimib.inf.ldp.hermes.services.ModelService;


@PowerMockIgnore({ "com.sun.org.apache.xerces.*", "javax.xml.*", "org.xml.*", "javax.management.*", "org.w3c.*" })
@RunWith(PowerMockRunner.class)
@PrepareForTest({ PayPalClientFactory.class })
public class PayPalOrderCreationCommandTest
{
    private static final String REMOTE_ORDER_CODE = "remote-order-code";

    private static final String TOTAL_PRICE = "12.45";

    private static final String CITY = "test_city";
    private static final String COUNTRY = "test_country";
    private static final String ADDRESS = "test_addr";
    private static final String ZIP = "test_zip";
    private static final String EMAIL = "test@email.com";
    private static final String NAME = "test_name";
    private static final String SURNAME = "test_sur";

    @Spy
    @InjectMocks
    private PayPalOrderCreationCommand classUnderTest = new PayPalOrderCreationCommand();

    @Mock
    private ModelService modelService;

    @Mock
    private PayPalHttpClient paypalClient;

    @Mock
    private HttpResponse<Order> paypalResponse;

    private CartModel cart;

    private ClientModel client;

    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);

        cart = new CartModel();
        PayPalPaymentInfoModel info = new PayPalPaymentInfoModel();
        info.setRemoteOrderCode(REMOTE_ORDER_CODE);
        cart.setPaymentInfo(info);

        PaymentModeModel mode = new PaymentModeModel();
        cart.setPaymentMode(mode);

        cart.setTotalPrice(new BigDecimal(TOTAL_PRICE));

        client = new ClientModel();

        given(modelService.rawMerge(cart)).willReturn(cart);

        PowerMockito.mockStatic(PayPalClientFactory.class);
        given(PayPalClientFactory.build(client, mode)).willReturn(paypalClient);
    }

    @Test
    public void testResultIsCompleted() throws IOException
    {
        //GIVEN
        given(paypalClient.execute(Mockito.any(OrdersGetRequest.class))).willReturn(paypalResponse);

        Order order = new Order();
        PurchaseUnit pu = new PurchaseUnit();
        AmountWithBreakdown amount = new AmountWithBreakdown();
        amount.value(TOTAL_PRICE);
        pu.amount(amount);

        order.purchaseUnits(Collections.singletonList(pu));
        given(paypalResponse.result()).willReturn(order);

        doNothing().when(classUnderTest).associateAddress(cart, order);

        // WHEN
        boolean result = classUnderTest.execute(cart, client);

        // THEN
        assertTrue(result);
    }

    @Test
    public void testResultIsNotCompleted() throws IOException
    {
        //GIVEN
        given(paypalClient.execute(Mockito.any(OrdersGetRequest.class))).willReturn(paypalResponse);

        Order order = new Order();
        PurchaseUnit pu = new PurchaseUnit();
        AmountWithBreakdown amount = new AmountWithBreakdown();
        amount.value("22");
        pu.amount(amount);

        order.purchaseUnits(Collections.singletonList(pu));
        given(paypalResponse.result()).willReturn(order);

        // WHEN
        boolean result = classUnderTest.execute(cart, client);

        // THEN
        assertFalse(result);
    }

    @Test
    public void testIOException() throws IOException
    {
        //GIVEN
        given(paypalClient.execute(Mockito.any(OrdersGetRequest.class))).willThrow(new IOException("This is a test"));

        // WHEN
        boolean result = classUnderTest.execute(cart, client);

        // THEN
        assertFalse(result);
    }

    @Test
    public void testNewAddress()
    {
        //GIVEN
        cart.setDeliveryAddress(null);
        Order order = createOrderForTest();

        // WHEN
        classUnderTest.associateAddress(cart, order);

        // THEN
        assertNotNull(cart.getDeliveryAddress());
        assertEquals(CITY, cart.getDeliveryAddress().getCity());
        assertEquals(COUNTRY, cart.getDeliveryAddress().getCountry());
        assertEquals(ADDRESS, cart.getDeliveryAddress().getStreet());
        assertEquals(ZIP, cart.getDeliveryAddress().getZipcode());
        assertEquals(NAME, cart.getDeliveryAddress().getFirstName());
        assertEquals(SURNAME, cart.getDeliveryAddress().getLastName());
        assertEquals(EMAIL, cart.getDeliveryAddress().getEmail());

    }


    @Test
    public void testAddress()
    {
        //GIVEN
        AddressModel address = new AddressModel();
        address.setFirstName(RandomStringUtils.randomAlphabetic(7));
        address.setFirstName(RandomStringUtils.randomAlphabetic(7));
        address.setEmail(RandomStringUtils.randomAlphabetic(7));
        cart.setDeliveryAddress(address);
        Order order = createOrderForTest();

        // WHEN
        classUnderTest.associateAddress(cart, order);

        // THEN
        assertNotNull(cart.getDeliveryAddress());
        assertEquals(CITY, cart.getDeliveryAddress().getCity());
        assertEquals(COUNTRY, cart.getDeliveryAddress().getCountry());
        assertEquals(ADDRESS, cart.getDeliveryAddress().getStreet());
        assertEquals(ZIP, cart.getDeliveryAddress().getZipcode());
        assertEquals(NAME, cart.getDeliveryAddress().getFirstName());
        assertEquals(SURNAME, cart.getDeliveryAddress().getLastName());
        assertEquals(EMAIL, cart.getDeliveryAddress().getEmail());

    }

    private Order createOrderForTest()
    {
        return new Order() //
                .purchaseUnits(Collections.singletonList( //
                        new PurchaseUnit().shipping( //
                                new ShippingDetails().addressPortable( //
                                        new AddressPortable() //
                                                .adminArea1(CITY) //
                                                .addressLine1(ADDRESS) //
                                                .postalCode(ZIP) //
                                                .countryCode(COUNTRY))))) //
                .payer(new Customer() //
                        .name( //
                                new Name().givenName(NAME) //
                                        .surname(SURNAME)) //
                        .emailAddress(EMAIL));

    }

}
