package it.unimib.inf.ldp.hermes.commands;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.BDDMockito.given;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.braintreepayments.http.HttpResponse;
import com.paypal.core.PayPalHttpClient;
import com.paypal.orders.Order;
import com.paypal.orders.OrdersCaptureRequest;

import it.unimib.inf.ldp.hermes.factories.PayPalClientFactory;
import it.unimib.inf.ldp.hermes.models.CartModel;
import it.unimib.inf.ldp.hermes.models.ClientModel;
import it.unimib.inf.ldp.hermes.models.PayPalPaymentInfoModel;
import it.unimib.inf.ldp.hermes.models.PaymentModeModel;

@PowerMockIgnore({"com.sun.org.apache.xerces.*", "javax.xml.*", "org.xml.*", "javax.management.*", "org.w3c.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({PayPalClientFactory.class})
public class PayPalCaptureCommandTest
{
    private static final String REMOTE_ORDER_CODE="remote-order-code";

    @InjectMocks
    private PayPalCaptureCommand classUnderTest;

    @Mock
    private CartModel cart;

    @Mock
    private ClientModel client;

    @Mock
    private PayPalHttpClient paypalClient;

    @Mock
    private PayPalPaymentInfoModel payPalPaymentInfo;

    @Mock
    private PaymentModeModel paymentMode;

    @Mock
    private HttpResponse<Order> paypalResponse;


    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);

        given(cart.getCode()).willReturn("TEST-CART");
        given(cart.getPaymentInfo()).willReturn(payPalPaymentInfo);
        given(cart.getPaymentMode()).willReturn(paymentMode);
    }

    @Test
    public void testResultIsCompleted() throws IOException
    {
        // GIVEN
        PowerMockito.mockStatic(PayPalClientFactory.class);
        given(PayPalClientFactory.build(client, paymentMode)).willReturn(paypalClient);
        given(paypalClient.execute(Mockito.any(OrdersCaptureRequest.class))).willReturn(paypalResponse);

        given(payPalPaymentInfo.getRemoteOrderCode()).willReturn(REMOTE_ORDER_CODE);

        Order order = new Order();
        order.status("COMPLETED");
        given(paypalResponse.result()).willReturn(order);

        // WHEN
        boolean result = classUnderTest.execute(cart, client);

        // THEN
        assertTrue(result);
    }

    @Test
    public void testResultIsNotCompleted() throws IOException
    {
        // GIVEN
        PowerMockito.mockStatic(PayPalClientFactory.class);
        given(PayPalClientFactory.build(client, paymentMode)).willReturn(paypalClient);
        given(paypalClient.execute(Mockito.any(OrdersCaptureRequest.class))).willReturn(paypalResponse);

        given(payPalPaymentInfo.getRemoteOrderCode()).willReturn(REMOTE_ORDER_CODE);

        Order order = new Order();
        order.status("STRING");
        given(paypalResponse.result()).willReturn(order);

        // WHEN
        boolean result = classUnderTest.execute(cart, client);

        // THEN
        assertFalse(result);
    }

    @Test
    public void testThrowsIOE() throws IOException
    {
        // GIVEN
        PowerMockito.mockStatic(PayPalClientFactory.class);
        given(PayPalClientFactory.build(client, paymentMode)).willReturn(paypalClient);
        given(paypalClient.execute(Mockito.any(OrdersCaptureRequest.class))).willThrow(new IOException("This is a test"));

        given(payPalPaymentInfo.getRemoteOrderCode()).willReturn(REMOTE_ORDER_CODE);

        // WHEN
        boolean result = classUnderTest.execute(cart, client);

        // THEN
        assertFalse(result);
    }

}
